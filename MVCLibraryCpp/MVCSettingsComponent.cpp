//MVCSettingsComponent.cpp

#include "stdafx.h"
#include "MVCSettingsComponent.h"

namespace MVCLibraryCpp 
{

#pragma region Declarations
#pragma endregion Declarations

#pragma region Constructors
		MVCSettingsComponent::MVCSettingsComponent()
		{
			__SomeOtherInt = 0;
			_SomeOtherInt = 0;
			__SomeOtherBoolean = false;
			_SomeOtherBoolean = false;
			__SomeOtherString = String::Empty; 
			_SomeOtherString = String::Empty; 
		}

		MVCSettingsComponent::MVCSettingsComponent(Int32 someOtherInt, Boolean someOtherBoolean, String^ someOtherString) 
		{
			MVCSettingsComponent::MVCSettingsComponent();//TODO:breakpoint the default ctor and see if this explicit call is necessary

			setSomeOtherInt(someOtherInt);
			setSomeOtherBoolean(someOtherBoolean);
			setSomeOtherString(someOtherString);
		}
#pragma endregion Constructors

#pragma region IDisposable support
	////C# TO C++ CONVERTER WARNING: There is no equivalent in C++ to finalizer methods:
	//	void MVCSettingsComponent::Finalize()
	//	{
	//		Dispose(false);
	//	}
	//	void MVCSettingsComponent::Dispose(Boolean disposeManagedResources)
	//	{
	//		// process only if mananged and unmanaged resources have
	//		// not been disposed of.
	//		if (!disposed)
	//		{
	//			try
	//			{
	//				//Resources not disposed
	//				if (disposeManagedResources)
	//				{
	//					// dispose managed resources
	//				}

	//				disposed = true;
	//			}
	////C# TO C++ CONVERTER TODO TASK: There is no C++ equivalent to the exception 'finally' clause:
	//			finally
	//			{
	//				// dispose unmanaged resources
	//				SettingsComponentBase::Dispose(disposeManagedResources);
	//			}
	//		}
	//		else
	//		{
	//			//Resources already disposed
	//		}
	//	}
#pragma endregion

#pragma region IEquatable<ISettingsComponent>
		/// <summary>
		/// Compare property values of two specified Settings objects.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		 Boolean MVCSettingsComponent::Equals(ISettingsComponent^ other) 
		{
			Boolean returnValue = false;
			MVCSettingsComponent^ otherModel = nullptr;

			try
			{
				otherModel = dynamic_cast<MVCSettingsComponent^>(other);

				if (this == otherModel)
				{
					returnValue = true;
				}
				else
				{
					if (!SettingsComponentBase::Equals(other))
					{
						returnValue = false;
					}
					else if (this->getSomeOtherInt() != otherModel->getSomeOtherInt())
					{
						returnValue = false;
					}
					else if (this->getSomeOtherBoolean() != otherModel->getSomeOtherBoolean())
					{
						returnValue = false;
					}
					else if (this->getSomeOtherString() != otherModel->getSomeOtherString())
					{
						returnValue = false;
					}
					else
					{
						returnValue = true;
					}
				}
			}
			catch (Exception^ ex)
			{
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);
				throw;
			}

			return returnValue;
		}
#pragma endregion IEquatable<ISettingsComponent>

#pragma region Properties
//C# TO C++ CONVERTER NOTE: The following .NET attribute has no direct equivalent in C++:
//ORIGINAL LINE: [XmlIgnore] public override Boolean Dirty
		 Boolean MVCSettingsComponent::getDirty()   
		{
			Boolean returnValue = false;

			try
			{
				if (SettingsComponentBase::Dirty)
				{
					returnValue = true;
				}
				else if (_SomeOtherInt != __SomeOtherInt)
				{
					returnValue = true;
				}
				else if (_SomeOtherBoolean != __SomeOtherBoolean)
				{
					returnValue = true;
				}
				else if (_SomeOtherString != __SomeOtherString)
				{
					returnValue = true;
				}
				else
				{
					returnValue = false;
				}
			}
			catch (Exception^ ex)
			{
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);
				throw;
			}

			return returnValue;
		}

#pragma region Persisted Properties
		Int32 MVCSettingsComponent::getSomeOtherInt() 
		{
			return _SomeOtherInt;
		}

		void MVCSettingsComponent::setSomeOtherInt(Int32 value)
		{
			_SomeOtherInt = value;
			OnPropertyChanged(L"SomeOtherInt");
		}

		Boolean MVCSettingsComponent::getSomeOtherBoolean() 
		{
			return _SomeOtherBoolean;
		}

		void MVCSettingsComponent::setSomeOtherBoolean(Boolean value)
		{
			_SomeOtherBoolean = value;
			OnPropertyChanged(L"SomeOtherBoolean");
		}

		String^ MVCSettingsComponent::getSomeOtherString() 
		{
			return _SomeOtherString;
		}

		void MVCSettingsComponent::setSomeOtherString(String^ value)
		{
			_SomeOtherString = value;
			OnPropertyChanged(L"SomeOtherString");
		}
#pragma endregion Persisted Properties
#pragma endregion Properties

#pragma region Methods
		/// <summary>
		/// Copies property values from source working fields to detination working fields, then optionally syncs destination.
		/// </summary>
		/// <param name="destination"></param>
		/// <param name="sync"></param>
		 void MVCSettingsComponent::CopyTo(ISettingsComponent^ destination, Boolean sync) 
		{
			MVCSettingsComponent^ destinationSettings = nullptr;

			try
			{
				destinationSettings = dynamic_cast<MVCSettingsComponent^>(destination);

				destinationSettings->setSomeOtherInt(this->getSomeOtherInt());
				destinationSettings->setSomeOtherBoolean(this->getSomeOtherBoolean());
				destinationSettings->setSomeOtherString(this->getSomeOtherString());

				SettingsComponentBase::CopyTo(destination, sync); //also checks and optionally performs sync
			}
			catch (Exception^ ex)
			{
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);
				throw;
			}
		}

		/// <summary>
		/// Syncs property values by copying from working fields to reference fields.
		/// </summary>
		 void MVCSettingsComponent::Sync() 
		{
			try
			{
				__SomeOtherInt = _SomeOtherInt;
				__SomeOtherBoolean = _SomeOtherBoolean;
				__SomeOtherString = _SomeOtherString;

				SettingsComponentBase::Sync();

				if (getDirty())
				{
					throw gcnew ApplicationException("Sync failed.");
				}
			}
			catch (Exception^ ex)
			{
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);
				throw;
			}
		}
#pragma endregion Methods

}
