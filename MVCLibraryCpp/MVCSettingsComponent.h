//MVCSettingsComponent.h

#pragma once

using namespace System;
using namespace System::Diagnostics;
using namespace System::Reflection;
using namespace Ssepan::Application;
using namespace Ssepan::Application::MVC;
using namespace Ssepan::Utility;

namespace MVCLibraryCpp 
{

    /// <summary>
    /// component of persisted settings; 
    /// run-time model depends on this;
    /// </summary>
//C# TO C++ CONVERTER TODO TASK: There is no C++ equivalent to the C# 'typeof' operator:
//C# TO C++ CONVERTER NOTE: The following .NET attribute has no direct equivalent in C++:
//ORIGINAL LINE: [TypeConverter(typeof(ExpandableObjectConverter))][Serializable] public class MVCSettingsComponent : SettingsComponentBase
	public ref class MVCSettingsComponent : public SettingsComponentBase
	{

#pragma region Declarations
#pragma endregion Declarations

#pragma region Constructors
	public:
		MVCSettingsComponent();

		MVCSettingsComponent(Int32 someOtherInt, Boolean someOtherBoolean, String^ someOtherString);
#pragma endregion Constructors

#pragma region IDisposable support
	private:
	////C# TO C++ CONVERTER WARNING: There is no equivalent in C++ to finalizer methods:
	//	void MVCSettingsComponent::Finalize();
	protected:
	//	void MVCSettingsComponent::Dispose(Boolean disposeManagedResources);
#pragma endregion

#pragma region IEquatable<ISettingsComponent>
		/// <summary>
		/// Compare property values of two specified Settings objects.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
	public:
		virtual Boolean Equals(ISettingsComponent^ other) override;
#pragma endregion IEquatable<ISettingsComponent>

#pragma region Properties
//C# TO C++ CONVERTER NOTE: The following .NET attribute has no direct equivalent in C++:
//ORIGINAL LINE: [XmlIgnore] public override Boolean Dirty
		virtual Boolean getDirty()  override; 

#pragma region Persisted Properties
	private:
		Int32 __SomeOtherInt;
		Int32 _SomeOtherInt;
	public:
		Int32 getSomeOtherInt() ;

		void setSomeOtherInt(Int32 value);

	private:
		Boolean __SomeOtherBoolean;
		Boolean _SomeOtherBoolean;
	public:
		Boolean getSomeOtherBoolean();

		void setSomeOtherBoolean(Boolean value);

	private:
		String^ __SomeOtherString;
		String^ _SomeOtherString;
	public:
		String^ getSomeOtherString();

		void setSomeOtherString(String^ value);
#pragma endregion Persisted Properties
#pragma endregion Properties

#pragma region Methods
		/// <summary>
		/// Copies property values from source working fields to detination working fields, then optionally syncs destination.
		/// </summary>
		/// <param name="destination"></param>
		/// <param name="sync"></param>
		virtual void CopyTo(ISettingsComponent^ destination, Boolean sync) override;

		/// <summary>
		/// Syncs property values by copying from working fields to reference fields.
		/// </summary>
		virtual void Sync() override;
#pragma endregion Methods

	};

}
