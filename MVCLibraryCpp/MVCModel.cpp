//MVCModel.cpp

#include "stdafx.h"
#include "MVCModel.h"

namespace MVCLibraryCpp {

		#pragma region Declarations
		#pragma endregion Declarations

		#pragma region Constructors
		MVCModel::MVCModel()
		{
			_Args = array<String^>();

			//init SomeComponent property backed by setting component
			if (SettingsController<MVCSettings^>::Settings == nullptr)
			{
				//ensures that there is a new instance of Settings backing persisted properties in model
				SettingsController<MVCSettings^>::New();

			}
			Debug::Assert((SettingsController<MVCSettings^>::Settings != nullptr));

			//init some other component property NOT backed by settings, but backed by model component
		   setStillAnotherComponent(gcnew MVCModelComponent());
		}

		MVCModel::MVCModel(Int32 someInt, Boolean someBoolean, String^ someString) 
		{
			MVCModel::MVCModel();

			setSomeInt(someInt);
			setSomeBoolean(someBoolean);
			setSomeString(someString);

		}

		MVCModel::MVCModel(Int32 someInt, Boolean someBoolean, String^ someString, Int32 someOtherInt, Boolean someOtherBoolean, String^ someOtherString) 
		{
			MVCModel::MVCModel(someInt,someBoolean,someString);

			getSomeComponent()->setSomeOtherInt(someOtherInt);
			getSomeComponent()->setSomeOtherBoolean(someOtherBoolean);
			getSomeComponent()->setSomeOtherString(someOtherString);

		}

		MVCModel::MVCModel(Int32 someInt, Boolean someBoolean, String^ someString, Int32 someOtherInt, Boolean someOtherBoolean, String^ someOtherString, Int32 stillAnotherInt, Boolean stillAnotherBoolean, String^ stillAnotherString) 
		{
			MVCModel::MVCModel(someInt,someBoolean,someString, someOtherInt, someOtherBoolean, someOtherString);

			getStillAnotherComponent()->setStillAnotherInt(stillAnotherInt);
			getStillAnotherComponent()->setStillAnotherBoolean(stillAnotherBoolean);
			getStillAnotherComponent()->setStillAnotherString(stillAnotherString);

		}

		MVCModel::~MVCModel()
		{
			delete _StillAnotherComponent;
		}
		#pragma endregion Constructors

		#pragma region IDisposable
////C# TO C++ CONVERTER WARNING: There is no equivalent in C++ to finalizer methods:
//		void MVCModel::Finalize()
//		{
//			Dispose(false);
//			//base.Finalize();//not called directly in C#; called by Destructor
//		}
//
//		//inherited; override if additional cleanup needed
//		void MVCModel::Dispose(Boolean disposeManagedResources) 
//		{
//			// process only if mananged and unmanaged resources have
//			// not been disposed of.
//			if (!disposed)
//			{
//				try
//				{
//					//Resources not disposed
//					if (disposeManagedResources)
//					{
//						// dispose managed resources
//						setStillAnotherComponent(nullptr);
//					}
//
//					disposed = true;
//				}
//	//C# TO C++ CONVERTER TODO TASK: There is no C++ equivalent to the exception 'finally' clause:
//				finally
//				{
//					// dispose unmanaged resources
//					ModelBase::Dispose(disposeManagedResources);
//				}
//			}
//			else
//			{
//				//Resources already disposed
//			}
//		}
		#pragma endregion IDisposable

		#pragma region IEquatable<IModel>
		/// <summary>
		/// Compare property values of two specified Model objects.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		Boolean MVCModel::Equals(IModelComponent^ other) 
		{
			Boolean returnValue = false;
			MVCModel^ otherModel = nullptr;

			try
			{
				otherModel = dynamic_cast<MVCModel^>(other);

				if (this == otherModel)
				{
					returnValue = true;
				}
				else
				{
					//if (!ModelBase::Equals(other))
					//{
					//	returnValue = false;
					//}
					/*else*/ if (this->getSomeInt() != otherModel->getSomeInt())
					{
						returnValue = false;
					}
					else if (this->getSomeBoolean() != otherModel->getSomeBoolean())
					{
						returnValue = false;
					}
					else if (this->getSomeString() != otherModel->getSomeString())
					{
						returnValue = false;
					}
					else
					{
						returnValue = true;
					}
				}
			}
			catch (Exception^ ex)
			{
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);
				throw;
			}

			return returnValue;
		}
		#pragma endregion IEquatable<IModel>

		#pragma region Properties
		array<String^>^ MVCModel::getArgs() 
		{
			return _Args;
		}

		void MVCModel::setArgs(array<String^>^ value)
		{
			_Args = value;
			OnPropertyChanged(L"Args");
		}


		MVCSettingsComponent^ MVCModel::getSomeComponent() 
		{
			return SettingsController<MVCSettings^>::Settings->getSomeComponent();
		}

		void MVCModel::setSomeComponent(MVCSettingsComponent^ value)
		{

			SettingsController<MVCSettings^>::Settings->setSomeComponent(value);
			//OnPropertyChanged("SomeComponent");//not needed if fired by settings
		}


		MVCModelComponent^ MVCModel::getStillAnotherComponent() 
		{
			return _StillAnotherComponent;
		}

		void MVCModel::setStillAnotherComponent(MVCModelComponent^ value)
		{

			if (ModelController<MVCModel^>::DefaultHandler != nullptr)
			{
				if (_StillAnotherComponent != nullptr)
				{
					_StillAnotherComponent->PropertyChanged -= ModelController<MVCModel^>::DefaultHandler;
				}
			}

			_StillAnotherComponent = value;

			if (ModelController<MVCModel^>::DefaultHandler != nullptr)
			{
				if (_StillAnotherComponent != nullptr)
				{
					_StillAnotherComponent->PropertyChanged += ModelController<MVCModel^>::DefaultHandler;
				}
			}

			OnPropertyChanged(L"StillAnotherComponent"); //needed because NOT backed by settings
		}


		Int32 MVCModel::getSomeInt() 
		{
			return SettingsController<MVCSettings^>::Settings->getSomeInt();
		}

		void MVCModel::setSomeInt(Int32 value)
		{
			SettingsController<MVCSettings^>::Settings->setSomeInt(value);
			//OnPropertyChanged("SomeInt");//not needed if fired by settings
		}


		Boolean MVCModel::getSomeBoolean() 
		{
			return SettingsController<MVCSettings^>::Settings->getSomeBoolean();
		}

		void MVCModel::setSomeBoolean(Boolean value)
		{
			SettingsController<MVCSettings^>::Settings->setSomeBoolean(value);
			//OnPropertyChanged("SomeBoolean");//not needed if fired by settings
		}


		String^  MVCModel::getSomeString() 
		{
			return SettingsController<MVCSettings^>::Settings->getSomeString();
		}

		void MVCModel::setSomeString(String^ value)
		{
			SettingsController<MVCSettings^>::Settings->setSomeString(value);
			//OnPropertyChanged("SomeString");//not needed if fired by settings
		}
		#pragma endregion Properties

		#pragma region Methods
		#pragma endregion Methods
}
