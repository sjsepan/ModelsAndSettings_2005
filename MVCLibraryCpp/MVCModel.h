//MVCModel.h

#pragma once

using namespace System;
using namespace System::Diagnostics;
using namespace System::Reflection;
using namespace Ssepan::Application;
using namespace Ssepan::Application::MVC;
using namespace Ssepan::Utility;

#using ".\Debug\MVCModelComponent.obj"
#using ".\Debug\MVCSettings.obj"
#using ".\Debug\MVCSettingsComponent.obj"

ref class MVCModelComponent;
ref class MVCSettings;
ref class MVCSettingsComponent;

namespace MVCLibraryCpp 
{

	/// <summary>
	/// run-time model; relies on settings
	/// </summary>
//C# TO C++ CONVERTER TODO TASK: There is no C++ equivalent to the C# 'typeof' operator:
//C# TO C++ CONVERTER NOTE: The following .NET attribute has no direct equivalent in C++:
//ORIGINAL LINE: [TypeConverter(typeof(ExpandableObjectConverter))] public class MVCModel : ModelBase
	public ref class MVCModel  : public ModelBase
	{


		#pragma region Declarations
		#pragma endregion Declarations

		#pragma region Constructors
	public:
		MVCModel();

		MVCModel(Int32 someInt, Boolean someBoolean, String^ someString);

		MVCModel(Int32 someInt, Boolean someBoolean, String^ someString, Int32 someOtherInt, Boolean someOtherBoolean, String^ someOtherString);

		MVCModel(Int32 someInt, Boolean someBoolean, String^ someString, Int32 someOtherInt, Boolean someOtherBoolean, String^ someOtherString, Int32 stillAnotherInt, Boolean stillAnotherBoolean, String^ stillAnotherString);

		virtual ~MVCModel();
		#pragma endregion Constructors

		#pragma region IDisposable
//	private:
////C# TO C++ CONVERTER WARNING: There is no equivalent in C++ to finalizer methods:
//		void Finalize();
//
//		//inherited; override if additional cleanup needed
//	protected:
//		void Dispose(Boolean disposeManagedResources) override;
		#pragma endregion IDisposable

		#pragma region IEquatable<IModel>
		/// <summary>
		/// Compare property values of two specified Model objects.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
	public:
		virtual Boolean Equals(IModelComponent^ other) override;
		#pragma endregion IEquatable<IModel>

		#pragma region Properties
	private:
		array<String^>^ _Args;
	public:
		array<String^>^ getArgs();

		void setArgs(array<String^>^ value);


		MVCSettingsComponent^ getSomeComponent();

		void setSomeComponent(MVCSettingsComponent^ value);

	private:
		MVCModelComponent^ _StillAnotherComponent;
	public:
		MVCModelComponent^ getStillAnotherComponent();

		void setStillAnotherComponent(MVCModelComponent^ value);


		Int32 getSomeInt();

		void setSomeInt(Int32 value);


		Boolean getSomeBoolean();

		void setSomeBoolean(Boolean value);


		String^ getSomeString();

		void setSomeString(String^ value);
		#pragma endregion Properties

		#pragma region Methods
		#pragma endregion Methods

	};

}