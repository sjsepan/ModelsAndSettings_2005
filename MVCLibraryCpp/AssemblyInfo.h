#pragma once
 
#define Assembly_Copyright L"Copyright (C) 1989, 1991 Free Software Foundation, Inc.  \n59 Temple Place - Suite 330, Boston, MA  02111-1307, USA"
#define Assembly_Description L"Reference implementation of MVC, Library"
#define File_Version 0.4.0.0
#define File_Version_Str "0.4.0.0"
 
//CLR assembly version
#define Assembly_Version L"0.4.0.0"
