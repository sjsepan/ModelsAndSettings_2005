//MVCSettings.h

#pragma once

using namespace System;
using namespace System::Diagnostics;
using namespace System::Reflection;
using namespace Ssepan::Application;
using namespace Ssepan::Application::MVC;
using namespace Ssepan::Utility;

#using ".\Debug\MVCSettingsComponent.obj"

ref class MVCSettingsComponent;

namespace MVCLibraryCpp 
{
	/// <summary>
	/// persisted settings; run-time model depends on this
	/// </summary>
//C# TO C++ CONVERTER TODO TASK: There is no C++ equivalent to the C# 'typeof' operator:
//C# TO C++ CONVERTER NOTE: The following .NET attribute has no direct equivalent in C++:
//ORIGINAL LINE: [TypeConverter(typeof(ExpandableObjectConverter))][Serializable] public class MVCSettings : SettingsBase
	public ref class MVCSettings : public SettingsBase 
	{
		#pragma region  Declarations
	private:
		static String^ FILE_TYPE_EXTENSION = L"mvcsettings";
		static String^ FILE_TYPE_NAME = L"mvcsettingsfile";
		static String^ FILE_TYPE_DESCRIPTION = L"MVC Settings File";
		#pragma endregion  Declarations

		#pragma region  Constructors
	public:

		MVCSettings();

		MVCSettings(Int32 someInt, Boolean someBoolean, String^ someString);

		MVCSettings(Int32 someInt, Boolean someBoolean, String^ someString, Int32 someOtherInt, Boolean someOtherBoolean, String^ someOtherString);

		MVCSettings(Int32 someInt, Boolean someBoolean, String^ someString, MVCSettingsComponent^ someComponent);

		virtual ~MVCSettings();
		#pragma endregion  Constructors

		#pragma region  IDisposable support
//	private:
////C# TO C++ CONVERTER WARNING: There is no equivalent in C++ to finalizer methods:
//		void Finalize();
//
//		//inherited; override if additional cleanup needed
//	protected:
//		virtual void Dispose(Boolean disposeManagedResources) override;
		#pragma endregion 

		#pragma region  IEquatable<ISettingsComponent>
		/// <summary>
		/// Compare property values of two specified Settings objects.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
	public:
		virtual Boolean Equals(ISettingsComponent^ other) override;
		#pragma endregion  IEquatable<ISettingsComponent>

		#pragma region  Properties
//C# TO C++ CONVERTER NOTE: The following .NET attribute has no direct equivalent in C++:
//ORIGINAL LINE: [XmlIgnore] public override Boolean Dirty
		Boolean getDirty();

		#pragma region  Persisted Properties
	private:
		//MVCSettingsComponent __SomeComponent;//not needed; individual properties are backed in settings
		MVCSettingsComponent^ _SomeComponent;
	public:
		MVCSettingsComponent^ getSomeComponent();

		void setSomeComponent(MVCSettingsComponent^ value);

	private:
		Int32 __SomeInt;
		Int32 _SomeInt;
	public:
		Int32 getSomeInt();

		void setSomeInt(Int32 value);

	private:
		Boolean __SomeBoolean;
		Boolean _SomeBoolean;
	public:
		Boolean getSomeBoolean();

		void setSomeBoolean(Boolean value);

	private:
		String^ __SomeString;
		String^ _SomeString;
	public:
		String^ getSomeString();
		
		void setSomeString(String^ value);
		#pragma endregion  Persisted Properties
		#pragma endregion  Properties

		#pragma region  Methods
		/// <summary>
		/// Copies property values from source working fields to detination working fields, then optionally syncs destination.
		/// </summary>
		/// <param name="destination"></param>
		/// <param name="sync"></param>
		virtual void CopyTo(ISettingsComponent^ destination, Boolean sync) override;

		/// <summary>
		/// Syncs property values by copying from working fields to reference fields.
		/// </summary>
		virtual void Sync() override;
		#pragma endregion  Methods

	};

}
