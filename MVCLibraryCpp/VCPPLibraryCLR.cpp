// VCPPLibraryCLR.cpp

// This is the main DLL file.

#include "stdafx.h"
#include "VCPPLibraryCLR.h"

namespace VCPPLIBRARYCLR {


	Class1::Class1()
	{
		this->greeting = L"Hello {0}";
	}

	String^ Class1::Hello(String^ name)
	{
		return String::Format(this->greeting, name);
	}
}
