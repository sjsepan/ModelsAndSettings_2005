//MVCModelComponent.cpp

#include "stdafx.h"
#include "MVCModelComponent.h"

namespace MVCLibraryCpp 
{
		#pragma region Declarations
		#pragma endregion Declarations

		#pragma region Constructors
		MVCModelComponent::MVCModelComponent()
		{
		}

		MVCModelComponent::MVCModelComponent(Int32 stillAnotherInt, Boolean stillAnotherBoolean, String^ stillAnotherString) 
		{
			MVCModelComponent::MVCModelComponent();//TODO:breakpoint the default ctor and see if this explicit call is necessary

			setStillAnotherInt(stillAnotherInt);
			setStillAnotherBoolean(stillAnotherBoolean);
			setStillAnotherString(stillAnotherString);
		}
		#pragma endregion Constructors

		#pragma region IDisposable support
////C# TO C++ CONVERTER WARNING: There is no equivalent in C++ to finalizer methods:
//		void MVCModelComponent::Finalize()
//		{
//			Dispose(false);
//		}
//
//		//inherited; override if additional cleanup needed
//		void MVCModelComponent::Dispose(Boolean disposeManagedResources)
//		{
//			// process only if mananged and unmanaged resources have
//			// not been disposed of.
//			if (!disposed)
//			{
//				try
//				{
//					//Resources not disposed
//					if (disposeManagedResources)
//					{
//						// dispose managed resources
//					}
//
//					disposed = true;
//				}
//	//C# TO C++ CONVERTER TODO TASK: There is no C++ equivalent to the exception 'finally' clause:
//				finally
//				{
//					// dispose unmanaged resources
//					ModelComponentBase::Dispose(disposeManagedResources);
//				}
//			}
//			else
//			{
//				//Resources already disposed
//			}
//		}
		#pragma endregion

		#pragma region IEquatable<IModelComponent>
		/// <summary>
		/// Compare property values of two specified Settings objects.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		Boolean MVCModelComponent::Equals(IModelComponent^ other)
		{
			Boolean returnValue = false;
			MVCModelComponent^ otherModel = nullptr;

			try
			{
				otherModel = dynamic_cast<MVCModelComponent^>(other);

				if (this == otherModel)
				{
					returnValue = true;
				}
				else
				{
					if (!ModelComponentBase::Equals(other))
					{
						returnValue = false;
					}
					else if (this->getStillAnotherInt() != otherModel->getStillAnotherInt())
					{
						returnValue = false;
					}
					else if (this->getStillAnotherBoolean() != otherModel->getStillAnotherBoolean())
					{
						returnValue = false;
					}
					else if (this->getStillAnotherString() != otherModel->getStillAnotherString())
					{
						returnValue = false;
					}
					else
					{
						returnValue = true;
					}
				}
			}
			catch (Exception^ ex)
			{
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);
				throw;
			}

			return returnValue;
		}
		#pragma endregion IEquatable<IModelComponent>
		
		#pragma region Properties
		#pragma region Non-Persisted Properties
		Int32 MVCModelComponent::getStillAnotherInt() 
		{
			return _StillAnotherInt;
		}

		void MVCModelComponent::setStillAnotherInt(Int32 value)
		{
			_StillAnotherInt = value;
			OnPropertyChanged(L"StillAnotherInt");
		}

		Boolean MVCModelComponent::getStillAnotherBoolean() 
		{
			return _StillAnotherBoolean;
		}

		void MVCModelComponent::setStillAnotherBoolean(Boolean value)
		{
			_StillAnotherBoolean = value;
			OnPropertyChanged(L"StillAnotherBoolean");
		}

		String^ MVCModelComponent::getStillAnotherString() 
		{
			return _StillAnotherString;
		}

		void MVCModelComponent::setStillAnotherString(String^ value)
		{
			_StillAnotherString = value;
			OnPropertyChanged(L"StillAnotherString");
		}
		#pragma endregion Non-Persisted Properties
		#pragma endregion Properties

		#pragma region Methods
		#pragma endregion Methods
}
