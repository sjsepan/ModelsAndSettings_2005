//MVCSettings.cpp

#include "stdafx.h"
#include "MVCSettings.h"

namespace MVCLibraryCpp 
{

		#pragma region  Declarations
	//private:
	//	static String^ FILE_TYPE_EXTENSION = L"mvcsettings";
	//	static String^ FILE_TYPE_NAME = L"mvcsettingsfile";
	//	static String^ FILE_TYPE_DESCRIPTION = L"MVC Settings File";
		#pragma endregion  Declarations

		#pragma region  Constructors
		MVCSettings::MVCSettings()
		{
			SettingsBase::FileTypeExtension = FILE_TYPE_EXTENSION;
			SettingsBase::FileTypeName = FILE_TYPE_NAME;
			SettingsBase::FileTypeDescription = FILE_TYPE_DESCRIPTION;
			SettingsBase::SerializeAs = SerializationFormat::Xml; //default

			setSomeComponent(gcnew MVCSettingsComponent());
		}

		MVCSettings::MVCSettings(Int32 someInt, Boolean someBoolean, String^ someString) 
		{
			MVCSettings::MVCSettings();//TODO:breakpoint the default ctor and see if this explicit call is necessary

			setSomeInt(someInt);
			setSomeBoolean(someBoolean);
			setSomeString(someString);
		}

		MVCSettings::MVCSettings(Int32 someInt, Boolean someBoolean, String^ someString, Int32 someOtherInt, Boolean someOtherBoolean, String^ someOtherString) 
		{
			MVCSettings::MVCSettings(someInt, someBoolean, someString);//TODO:breakpoint the default ctor and see if this explicit call is necessary

 			getSomeComponent()->setSomeOtherInt(someOtherInt);
			getSomeComponent()->setSomeOtherBoolean(someOtherBoolean);
			getSomeComponent()->setSomeOtherString(someOtherString);
		}

		MVCSettings::MVCSettings(Int32 someInt, Boolean someBoolean, String^ someString, MVCSettingsComponent^ someComponent) 
		{
			MVCSettings::MVCSettings(someInt, someBoolean, someString);//TODO:breakpoint the default ctor and see if this explicit call is necessary

			setSomeComponent(someComponent);
		}

		/*virtual*/ MVCSettings::~MVCSettings()
		{
			delete _SomeComponent;
		}
		#pragma endregion  Constructors

		#pragma region  IDisposable support
////C# TO C++ CONVERTER WARNING: There is no equivalent in C++ to finalizer methods:
//		void MVCSettings::Finalize()
//		{
//			Dispose(false);
//			//base.Finalize();//not called directly in C#; called by Destructor
//		}
//
//		//inherited; override if additional cleanup needed

//		void MVCSettings::Dispose(Boolean disposeManagedResources)
//		{
//			// process only if mananged and unmanaged resources have
//			// not been disposed of.
//			if (!disposed)
//			{
//				try
//				{
//					//Resources not disposed
//					if (disposeManagedResources)
//					{
//						// dispose managed resources
//						setSomeComponent(nullptr);
//					}
//
//					disposed = true;
//				}
//	//C# TO C++ CONVERTER TODO TASK: There is no C++ equivalent to the exception 'finally' clause:
//				finally
//				{
//					// dispose unmanaged resources
//					SettingsBase::Dispose(disposeManagedResources);
//				}
//			}
//			else
//			{
//				//Resources already disposed
//			}
//		}
		#pragma endregion 

		#pragma region  IEquatable<ISettingsComponent>
		/// <summary>
		/// Compare property values of two specified Settings objects.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		Boolean MVCSettings::Equals(ISettingsComponent^ other)
		{
			Boolean returnValue = false;
			MVCSettings^ otherSettings = nullptr;

			try
			{
				otherSettings = dynamic_cast<MVCSettings^>(other);

				if (this == otherSettings)
				{
					returnValue = true;
				}
				else
				{
					if (!SettingsBase::Equals(other))
					{
						returnValue = false;
					}
					else if (!this->getSomeComponent()->Equals(otherSettings))
					{
						returnValue = false;
					}
					else if (this->getSomeInt() != otherSettings->getSomeInt())
					{
						returnValue = false;
					}
					else if (this->getSomeBoolean() != otherSettings->getSomeBoolean())
					{
						returnValue = false;
					}
					else if (this->getSomeString() != otherSettings->getSomeString())
					{
						returnValue = false;
					}
					else
					{
						returnValue = true;
					}
				}
			}
			catch (Exception^ ex)
			{
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);
				throw;
			}

			return returnValue;
		}
		#pragma endregion  IEquatable<ISettingsComponent>

		#pragma region  Properties
//C# TO C++ CONVERTER NOTE: The following .NET attribute has no direct equivalent in C++:
//ORIGINAL LINE: [XmlIgnore] public override Boolean Dirty
		Boolean MVCSettings::getDirty()
		{
			Boolean returnValue = false;

			try
			{
				if (SettingsBase::Dirty)
				{
					returnValue = true;
				}
				//else if (_SomeComponent.Equals(__SomeComponent))
				else if (getSomeComponent()->getDirty())
				{
					returnValue = true;
				}
				else if (_SomeInt != __SomeInt)
				{
					returnValue = true;
				}
				else if (_SomeBoolean != __SomeBoolean)
				{
					returnValue = true;
				}
				else if (_SomeString != __SomeString)
				{
					returnValue = true;
				}
				else
				{
					returnValue = false;
				}
			}
			catch (Exception^ ex)
			{
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);
				throw;
			}

			return returnValue;
		}

		#pragma region  Persisted Properties
		MVCSettingsComponent^ MVCSettings::getSomeComponent() 
		{
			return _SomeComponent;
		}

		void MVCSettings::setSomeComponent(MVCSettingsComponent^ value)
		{
			if (SettingsController<MVCSettings^>::DefaultHandler != nullptr)
			{
				if (_SomeComponent != nullptr)
				{
					_SomeComponent->PropertyChanged -= SettingsController<MVCSettings^>::DefaultHandler;
				}
			}

			_SomeComponent = value;

			if (SettingsController<MVCSettings^>::DefaultHandler != nullptr)
			{
				if (_SomeComponent != nullptr)
				{
					_SomeComponent->PropertyChanged += SettingsController<MVCSettings^>::DefaultHandler;
				}
			}

			OnPropertyChanged(L"SomeComponent");
		}

		Int32 MVCSettings::getSomeInt() 
		{
			return _SomeInt;
		}

		void MVCSettings::setSomeInt(Int32 value)
		{
			_SomeInt = value;
			OnPropertyChanged(L"SomeInt");
		}

		Boolean MVCSettings::getSomeBoolean() 
		{
			return _SomeBoolean;
		}

		void MVCSettings::setSomeBoolean(Boolean value)
		{
			_SomeBoolean = value;
			OnPropertyChanged(L"SomeBoolean");
		}

		String^ MVCSettings::getSomeString() 
		{
			return _SomeString;
		}
		
		void MVCSettings::setSomeString(String^ value)
		{
			_SomeString = value;
			OnPropertyChanged(L"SomeString");
		}
		#pragma endregion  Persisted Properties
		#pragma endregion  Properties

		#pragma region  Methods
		/// <summary>
		/// Copies property values from source working fields to detination working fields, then optionally syncs destination.
		/// </summary>
		/// <param name="destination"></param>
		/// <param name="sync"></param>
		void MVCSettings::CopyTo(ISettingsComponent^ destination, Boolean sync)
		{
			MVCSettings^ destinationSettings = nullptr;

			try
			{
				destinationSettings = dynamic_cast<MVCSettings^>(destination);

				//destinationSettings.SomeComponent = this.SomeComponent;
				this->getSomeComponent()->CopyTo(destinationSettings->getSomeComponent(), sync);

				destinationSettings->setSomeInt(this->getSomeInt());
				destinationSettings->setSomeBoolean(this->getSomeBoolean());
				destinationSettings->setSomeString(this->getSomeString());

				SettingsBase::CopyTo(destination, sync); //also checks and optionally performs sync
			}
			catch (Exception^ ex)
			{
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);
				throw;
			}
		}

		/// <summary>
		/// Syncs property values by copying from working fields to reference fields.
		/// </summary>
		void MVCSettings::Sync() 
		{
			try
			{
				//__SomeComponent = ObjectHelper.Clone<MVCSettingsComponent>(_SomeComponent);
				getSomeComponent()->Sync();

				__SomeInt = _SomeInt;
				__SomeBoolean = _SomeBoolean;
				__SomeString = _SomeString;

				SettingsBase::Sync();

				if (Dirty)
				{
					throw gcnew ApplicationException("Sync failed.");
				}
			}
			catch (Exception^ ex)
			{
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);
				throw;
			}
		}
		#pragma endregion  Methods
}
