//MVCModelComponent.h

#pragma once

using namespace System;
using namespace System::Diagnostics;
using namespace System::Reflection;
using namespace Ssepan::Application;
using namespace Ssepan::Application::MVC;
using namespace Ssepan::Utility;

namespace MVCLibraryCpp 
{

	/// <summary>
	/// component of non-persisted properties; 
	/// run-time model depends on this;
	/// does NOT rely on settings
	/// </summary>
//C# TO C++ CONVERTER TODO TASK: There is no C++ equivalent to the C# 'typeof' operator:
//C# TO C++ CONVERTER NOTE: The following .NET attribute has no direct equivalent in C++:
//ORIGINAL LINE: [TypeConverter(typeof(ExpandableObjectConverter))] public class MVCModelComponent : ModelComponentBase
	public ref class MVCModelComponent : public ModelComponentBase 
	{

		#pragma region Declarations
		#pragma endregion Declarations

		#pragma region Constructors
	public:
		MVCModelComponent();

		MVCModelComponent(Int32 stillAnotherInt, Boolean stillAnotherBoolean, String^ stillAnotherString);
		#pragma endregion Constructors

		#pragma region IDisposable support
//	private:
////C# TO C++ CONVERTER WARNING: There is no equivalent in C++ to finalizer methods:
//		void Finalize();
//
//		//inherited; override if additional cleanup needed
//	protected:
//		void Dispose(Boolean disposeManagedResources);
		#pragma endregion

		#pragma region IEquatable<IModelComponent>
		/// <summary>
		/// Compare property values of two specified Settings objects.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
	public:
		virtual Boolean Equals(IModelComponent^ other) override;
		#pragma endregion IEquatable<IModelComponent>
		
		#pragma region Properties
		#pragma region Non-Persisted Properties
	private:
		Int32 _StillAnotherInt;
	public:
		Int32 getStillAnotherInt();

		void setStillAnotherInt(Int32 value);

	private:
		Boolean _StillAnotherBoolean;
	public:
		Boolean getStillAnotherBoolean();

		void setStillAnotherBoolean(Boolean value);

	private:
		String^ _StillAnotherString;
	public:
		String^ getStillAnotherString();

		void setStillAnotherString(String^ value);
		#pragma endregion Non-Persisted Properties
		#pragma endregion Properties

		#pragma region Methods
		#pragma endregion Methods

	};
}

