//Globals.h

#pragma once

using namespace System;
using namespace System::Diagnostics;
using namespace System::Reflection;
using namespace Ssepan::Utility;

//namespace MVCFormsCpp
//{
	public ref class Globals abstract sealed
	{
		#pragma region Declarations
	public:
		static String^ APP_NAME;// = L"MVCFormsCpp";
		#pragma endregion Declarations

		#pragma region Constructors
	public:
		static Globals();
		#pragma endregion Constructors

		#pragma region Properties
	private:
		static String^ _Filename;
	public:
		static String^ getFilename();

		static void setFilename(String^ value);
		#pragma endregion Properties
	};
//}
