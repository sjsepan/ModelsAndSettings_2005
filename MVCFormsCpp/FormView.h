//FormView.h

#pragma once

//#define USE_CONFIG_FILEPATH
//#define USE_CUSTOM_VIEWMODEL
//#define DEBUG_MODEL_PROPERTYCHANGED
//#define DEBUG_SETTINGS_PROPERTYCHANGED

#include "MVCViewModel.h"

using namespace System;
using namespace System::Collections;
using namespace System::ComponentModel;
using namespace System::Diagnostics;
using namespace System::Reflection;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace Ssepan::Application;
using namespace Ssepan::Application::MVC;
using namespace Ssepan::Application::WinForms;
using namespace Ssepan::Io;
using namespace Ssepan::Utility;
using namespace MVCLibraryCpp;
using namespace VCPPLIBRARYCLR;

#using ".\Debug\Globals.obj"
#using ".\Debug\MVCViewModel.obj"

ref class Globals;
ref class MVCViewModel;

namespace MVCFormsCpp {

	/// <summary>
	/// Summary for FormView
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class FormView : public System::Windows::Forms::Form//, public INotifyPropertyChanged
	{
		#pragma region Declarations
	protected:
		Boolean disposed;// = false;
		static MVCViewModel^ ViewModel;// = nullptr;

	private:
		static Boolean _ValueChangedProgrammatically;// = false;

		//cancellation hook
		//TODO:System::Action^ cancelDelegate;// = nullptr;
		#pragma endregion Declarations

		#pragma region Constructors
	public:
		FormView()
		{
            try
            {
				InitializeComponent();

				_ViewName = Globals::APP_NAME;

				////(re)define default output delegate
				//ConsoleApplication.defaultOutputDelegate = ConsoleApplication.messageBoxWrapperOutputDelegate;

				//subscribe to view's notifications
				this->PropertyChanged += PropertyChangedEventHandlerDelegate;

				InitViewModel();

				BindSizeAndLocation();
            }
            catch (Exception^ ex)
            {
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);
            }
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~FormView()
		{
			if (components)
			{
				delete components;
			}
		}
		#pragma endregion Constructors

	private: System::Windows::Forms::CheckBox^  chkStillAnotherBoolean;
	private: System::Windows::Forms::TextBox^  txtStillAnotherString;
	private: System::Windows::Forms::TextBox^  txtStillAnotherInt;
	private: System::Windows::Forms::Label^  lblStillAnotherString;
	private: System::Windows::Forms::Label^  lblStillAnotherInt;
	private: System::Windows::Forms::CheckBox^  chkSomeOtherBoolean;
	private: System::Windows::Forms::TextBox^  txtSomeOtherString;
	private: System::Windows::Forms::TextBox^  txtSomeOtherInt;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Button^  cmdRun;
	private: System::Windows::Forms::CheckBox^  chkSomeBoolean;
	private: System::Windows::Forms::TextBox^  txtSomeString;
	private: System::Windows::Forms::TextBox^  txtSomeInt;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::MenuStrip^  menu;
	private: System::Windows::Forms::ToolStripMenuItem^  menuFile;
	private: System::Windows::Forms::ToolStripMenuItem^  menuFileNew;
	private: System::Windows::Forms::ToolStripMenuItem^  menuFileOpen;
	private: System::Windows::Forms::ToolStripMenuItem^  menuFileSave;
	private: System::Windows::Forms::ToolStripMenuItem^  menuFileSaveAs;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator1;
	private: System::Windows::Forms::ToolStripMenuItem^  menuFilePrint;
	private: System::Windows::Forms::ToolStripSeparator^  menuFileSeparator2;
	private: System::Windows::Forms::ToolStripMenuItem^  menuFileExit;
	private: System::Windows::Forms::ToolStripMenuItem^  menuEdit;
	private: System::Windows::Forms::ToolStripMenuItem^  menuEditUndo;
	private: System::Windows::Forms::ToolStripMenuItem^  menuEditRedo;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator4;
	private: System::Windows::Forms::ToolStripMenuItem^  menuEditSelectAll;
	private: System::Windows::Forms::ToolStripMenuItem^  menuEditCut;
	private: System::Windows::Forms::ToolStripMenuItem^  menuEditCopy;
	private: System::Windows::Forms::ToolStripMenuItem^  menuEditPaste;
	private: System::Windows::Forms::ToolStripMenuItem^  menuEditDelete;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator5;
	private: System::Windows::Forms::ToolStripMenuItem^  menuEditFind;
	private: System::Windows::Forms::ToolStripMenuItem^  menuEditReplace;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator6;
	private: System::Windows::Forms::ToolStripMenuItem^  menuEditRefresh;
	private: System::Windows::Forms::ToolStripSeparator^  menuEditSeparator0;
	private: System::Windows::Forms::ToolStripMenuItem^  menuEditPreferences;
	private: System::Windows::Forms::ToolStripMenuItem^  menuEditProperties;
	private: System::Windows::Forms::ToolStripMenuItem^  menuHelp;
	private: System::Windows::Forms::ToolStripMenuItem^  menuHelpContents;
	private: System::Windows::Forms::ToolStripMenuItem^  menuHelpIndex;
	private: System::Windows::Forms::ToolStripMenuItem^  menuHelpOnlineHelp;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator3;
	private: System::Windows::Forms::ToolStripMenuItem^  menuHelpLicenceInformation;
	private: System::Windows::Forms::ToolStripMenuItem^  menuHelpCheckForUpdates;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator2;
	private: System::Windows::Forms::ToolStripMenuItem^  menuHelpAbout;
	private: System::Windows::Forms::ToolStrip^  toolbar;
	private: System::Windows::Forms::ToolStripButton^  buttonFileNew;
	private: System::Windows::Forms::ToolStripButton^  buttonFileOpen;
	private: System::Windows::Forms::ToolStripButton^  buttonFileSave;
	private: System::Windows::Forms::ToolStripButton^  buttonFilePrint;
	private: System::Windows::Forms::ToolStripSeparator^  buttonSeparator0;
	private: System::Windows::Forms::ToolStripButton^  buttonEditUndo;
	private: System::Windows::Forms::ToolStripButton^  buttonEditRedo;
	private: System::Windows::Forms::ToolStripButton^  buttonEditCut;
	private: System::Windows::Forms::ToolStripButton^  buttonEditCopy;
	private: System::Windows::Forms::ToolStripButton^  buttonEditPaste;
	private: System::Windows::Forms::ToolStripButton^  buttonEditDelete;
	private: System::Windows::Forms::ToolStripButton^  buttonEditFind;
	private: System::Windows::Forms::ToolStripButton^  buttonEditReplace;
	private: System::Windows::Forms::ToolStripButton^  buttonEditRefresh;
	private: System::Windows::Forms::ToolStripButton^  buttonEditPreferences;
	private: System::Windows::Forms::ToolStripButton^  buttonEditProperties;
	private: System::Windows::Forms::ToolStripSeparator^  buttonSeparator1;
	private: System::Windows::Forms::ToolStripButton^  buttonHelpContents;
	private: System::Windows::Forms::StatusStrip^  statusBar;
	private: System::Windows::Forms::ToolStripStatusLabel^  StatusBarStatusMessage;
	private: System::Windows::Forms::ToolStripStatusLabel^  StatusBarErrorMessage;
	private: System::Windows::Forms::ToolStripProgressBar^  StatusBarProgressBar;
	private: System::Windows::Forms::ToolStripStatusLabel^  StatusBarActionIcon;
	private: System::Windows::Forms::ToolStripStatusLabel^  StatusBarDirtyMessage;
	private: System::Windows::Forms::ToolStripStatusLabel^  StatusBarNetworkIndicator;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(FormView::typeid));
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->menu = (gcnew System::Windows::Forms::MenuStrip());
			this->menuFile = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuFileNew = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuFileOpen = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuFileSave = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuFileSaveAs = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator1 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->menuFilePrint = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuFileSeparator2 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->menuFileExit = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuEdit = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuEditUndo = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuEditRedo = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator4 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->menuEditSelectAll = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuEditCut = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuEditCopy = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuEditPaste = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuEditDelete = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator5 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->menuEditFind = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuEditReplace = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator6 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->menuEditRefresh = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuEditSeparator0 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->menuEditPreferences = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuEditProperties = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuHelp = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuHelpContents = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuHelpIndex = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuHelpOnlineHelp = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator3 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->menuHelpLicenceInformation = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuHelpCheckForUpdates = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator2 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->menuHelpAbout = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolbar = (gcnew System::Windows::Forms::ToolStrip());
			this->buttonFileNew = (gcnew System::Windows::Forms::ToolStripButton());
			this->buttonFileOpen = (gcnew System::Windows::Forms::ToolStripButton());
			this->buttonFileSave = (gcnew System::Windows::Forms::ToolStripButton());
			this->buttonFilePrint = (gcnew System::Windows::Forms::ToolStripButton());
			this->buttonSeparator0 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->buttonEditUndo = (gcnew System::Windows::Forms::ToolStripButton());
			this->buttonEditRedo = (gcnew System::Windows::Forms::ToolStripButton());
			this->buttonEditCut = (gcnew System::Windows::Forms::ToolStripButton());
			this->buttonEditCopy = (gcnew System::Windows::Forms::ToolStripButton());
			this->buttonEditPaste = (gcnew System::Windows::Forms::ToolStripButton());
			this->buttonEditDelete = (gcnew System::Windows::Forms::ToolStripButton());
			this->buttonEditFind = (gcnew System::Windows::Forms::ToolStripButton());
			this->buttonEditReplace = (gcnew System::Windows::Forms::ToolStripButton());
			this->buttonEditRefresh = (gcnew System::Windows::Forms::ToolStripButton());
			this->buttonEditPreferences = (gcnew System::Windows::Forms::ToolStripButton());
			this->buttonEditProperties = (gcnew System::Windows::Forms::ToolStripButton());
			this->buttonSeparator1 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->buttonHelpContents = (gcnew System::Windows::Forms::ToolStripButton());
			this->statusBar = (gcnew System::Windows::Forms::StatusStrip());
			this->StatusBarStatusMessage = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->StatusBarErrorMessage = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->StatusBarProgressBar = (gcnew System::Windows::Forms::ToolStripProgressBar());
			this->StatusBarActionIcon = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->StatusBarDirtyMessage = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->StatusBarNetworkIndicator = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->chkStillAnotherBoolean = (gcnew System::Windows::Forms::CheckBox());
			this->txtStillAnotherString = (gcnew System::Windows::Forms::TextBox());
			this->txtStillAnotherInt = (gcnew System::Windows::Forms::TextBox());
			this->lblStillAnotherString = (gcnew System::Windows::Forms::Label());
			this->lblStillAnotherInt = (gcnew System::Windows::Forms::Label());
			this->chkSomeOtherBoolean = (gcnew System::Windows::Forms::CheckBox());
			this->txtSomeOtherString = (gcnew System::Windows::Forms::TextBox());
			this->txtSomeOtherInt = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->cmdRun = (gcnew System::Windows::Forms::Button());
			this->chkSomeBoolean = (gcnew System::Windows::Forms::CheckBox());
			this->txtSomeString = (gcnew System::Windows::Forms::TextBox());
			this->txtSomeInt = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->menu->SuspendLayout();
			this->toolbar->SuspendLayout();
			this->statusBar->SuspendLayout();
			this->SuspendLayout();
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(77, 231);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(100, 20);
			this->textBox1->TabIndex = 5;
			this->textBox1->Text = L"bob";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(6, 234);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(64, 13);
			this->label1->TabIndex = 4;
			this->label1->Text = L"Enter name:";
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(191, 229);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 3;
			this->button1->Text = L"button1";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &FormView::button1_Click);
			// 
			// menu
			// 
			this->menu->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->menuFile, this->menuEdit, 
				this->menuHelp});
			this->menu->Location = System::Drawing::Point(0, 0);
			this->menu->Name = L"menu";
			this->menu->Size = System::Drawing::Size(624, 24);
			this->menu->TabIndex = 118;
			this->menu->Text = L"menuStrip1";
			// 
			// menuFile
			// 
			this->menuFile->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(8) {this->menuFileNew, 
				this->menuFileOpen, this->menuFileSave, this->menuFileSaveAs, this->toolStripSeparator1, this->menuFilePrint, this->menuFileSeparator2, 
				this->menuFileExit});
			this->menuFile->Name = L"menuFile";
			this->menuFile->Size = System::Drawing::Size(37, 20);
			this->menuFile->Text = L"&File";
			// 
			// menuFileNew
			// 
			this->menuFileNew->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"menuFileNew.Image")));
			this->menuFileNew->Name = L"menuFileNew";
			this->menuFileNew->Size = System::Drawing::Size(149, 22);
			this->menuFileNew->Text = L"&New";
			this->menuFileNew->Click += gcnew System::EventHandler(this, &FormView::menuFileNew_Click);
			// 
			// menuFileOpen
			// 
			this->menuFileOpen->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"menuFileOpen.Image")));
			this->menuFileOpen->Name = L"menuFileOpen";
			this->menuFileOpen->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::O));
			this->menuFileOpen->Size = System::Drawing::Size(149, 22);
			this->menuFileOpen->Text = L"&Open";
			this->menuFileOpen->Click += gcnew System::EventHandler(this, &FormView::menuFileOpen_Click);
			// 
			// menuFileSave
			// 
			this->menuFileSave->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"menuFileSave.Image")));
			this->menuFileSave->Name = L"menuFileSave";
			this->menuFileSave->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::S));
			this->menuFileSave->Size = System::Drawing::Size(149, 22);
			this->menuFileSave->Text = L"&Save";
			this->menuFileSave->Click += gcnew System::EventHandler(this, &FormView::menuFileSave_Click);
			// 
			// menuFileSaveAs
			// 
			this->menuFileSaveAs->Name = L"menuFileSaveAs";
			this->menuFileSaveAs->Size = System::Drawing::Size(149, 22);
			this->menuFileSaveAs->Text = L"Save &As...";
			// 
			// toolStripSeparator1
			// 
			this->toolStripSeparator1->Name = L"toolStripSeparator1";
			this->toolStripSeparator1->Size = System::Drawing::Size(146, 6);
			// 
			// menuFilePrint
			// 
			this->menuFilePrint->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"menuFilePrint.Image")));
			this->menuFilePrint->Name = L"menuFilePrint";
			this->menuFilePrint->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::P));
			this->menuFilePrint->Size = System::Drawing::Size(149, 22);
			this->menuFilePrint->Text = L"&Print...";
			// 
			// menuFileSeparator2
			// 
			this->menuFileSeparator2->Name = L"menuFileSeparator2";
			this->menuFileSeparator2->Size = System::Drawing::Size(146, 6);
			// 
			// menuFileExit
			// 
			this->menuFileExit->Name = L"menuFileExit";
			this->menuFileExit->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::Q));
			this->menuFileExit->Size = System::Drawing::Size(149, 22);
			this->menuFileExit->Text = L"E&xit";
			// 
			// menuEdit
			// 
			this->menuEdit->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(16) {this->menuEditUndo, 
				this->menuEditRedo, this->toolStripSeparator4, this->menuEditSelectAll, this->menuEditCut, this->menuEditCopy, this->menuEditPaste, 
				this->menuEditDelete, this->toolStripSeparator5, this->menuEditFind, this->menuEditReplace, this->toolStripSeparator6, this->menuEditRefresh, 
				this->menuEditSeparator0, this->menuEditPreferences, this->menuEditProperties});
			this->menuEdit->Name = L"menuEdit";
			this->menuEdit->Size = System::Drawing::Size(39, 20);
			this->menuEdit->Text = L"&Edit";
			// 
			// menuEditUndo
			// 
			this->menuEditUndo->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"menuEditUndo.Image")));
			this->menuEditUndo->Name = L"menuEditUndo";
			this->menuEditUndo->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::Z));
			this->menuEditUndo->Size = System::Drawing::Size(167, 22);
			this->menuEditUndo->Text = L"&Undo";
			// 
			// menuEditRedo
			// 
			this->menuEditRedo->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"menuEditRedo.Image")));
			this->menuEditRedo->Name = L"menuEditRedo";
			this->menuEditRedo->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::Y));
			this->menuEditRedo->Size = System::Drawing::Size(167, 22);
			this->menuEditRedo->Text = L"&Redo";
			// 
			// toolStripSeparator4
			// 
			this->toolStripSeparator4->Name = L"toolStripSeparator4";
			this->toolStripSeparator4->Size = System::Drawing::Size(164, 6);
			// 
			// menuEditSelectAll
			// 
			this->menuEditSelectAll->Name = L"menuEditSelectAll";
			this->menuEditSelectAll->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::A));
			this->menuEditSelectAll->Size = System::Drawing::Size(167, 22);
			this->menuEditSelectAll->Text = L"Select &All";
			// 
			// menuEditCut
			// 
			this->menuEditCut->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"menuEditCut.Image")));
			this->menuEditCut->Name = L"menuEditCut";
			this->menuEditCut->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::X));
			this->menuEditCut->Size = System::Drawing::Size(167, 22);
			this->menuEditCut->Text = L"Cu&t";
			// 
			// menuEditCopy
			// 
			this->menuEditCopy->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"menuEditCopy.Image")));
			this->menuEditCopy->ImageTransparentColor = System::Drawing::Color::Black;
			this->menuEditCopy->Name = L"menuEditCopy";
			this->menuEditCopy->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::C));
			this->menuEditCopy->Size = System::Drawing::Size(167, 22);
			this->menuEditCopy->Text = L"&Copy";
			// 
			// menuEditPaste
			// 
			this->menuEditPaste->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"menuEditPaste.Image")));
			this->menuEditPaste->Name = L"menuEditPaste";
			this->menuEditPaste->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::V));
			this->menuEditPaste->Size = System::Drawing::Size(167, 22);
			this->menuEditPaste->Text = L"&Paste";
			// 
			// menuEditDelete
			// 
			this->menuEditDelete->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"menuEditDelete.Image")));
			this->menuEditDelete->Name = L"menuEditDelete";
			this->menuEditDelete->ShortcutKeys = System::Windows::Forms::Keys::Delete;
			this->menuEditDelete->Size = System::Drawing::Size(167, 22);
			this->menuEditDelete->Text = L"&Delete";
			// 
			// toolStripSeparator5
			// 
			this->toolStripSeparator5->Name = L"toolStripSeparator5";
			this->toolStripSeparator5->Size = System::Drawing::Size(164, 6);
			// 
			// menuEditFind
			// 
			this->menuEditFind->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"menuEditFind.Image")));
			this->menuEditFind->Name = L"menuEditFind";
			this->menuEditFind->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::F));
			this->menuEditFind->Size = System::Drawing::Size(167, 22);
			this->menuEditFind->Text = L"&Find...";
			// 
			// menuEditReplace
			// 
			this->menuEditReplace->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"menuEditReplace.Image")));
			this->menuEditReplace->Name = L"menuEditReplace";
			this->menuEditReplace->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::H));
			this->menuEditReplace->Size = System::Drawing::Size(167, 22);
			this->menuEditReplace->Text = L"Rep&lace...";
			// 
			// toolStripSeparator6
			// 
			this->toolStripSeparator6->Name = L"toolStripSeparator6";
			this->toolStripSeparator6->Size = System::Drawing::Size(164, 6);
			// 
			// menuEditRefresh
			// 
			this->menuEditRefresh->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"menuEditRefresh.Image")));
			this->menuEditRefresh->Name = L"menuEditRefresh";
			this->menuEditRefresh->ShortcutKeys = System::Windows::Forms::Keys::F5;
			this->menuEditRefresh->Size = System::Drawing::Size(167, 22);
			this->menuEditRefresh->Text = L"R&efresh";
			// 
			// menuEditSeparator0
			// 
			this->menuEditSeparator0->Name = L"menuEditSeparator0";
			this->menuEditSeparator0->Size = System::Drawing::Size(164, 6);
			// 
			// menuEditPreferences
			// 
			this->menuEditPreferences->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"menuEditPreferences.Image")));
			this->menuEditPreferences->Name = L"menuEditPreferences";
			this->menuEditPreferences->Size = System::Drawing::Size(167, 22);
			this->menuEditPreferences->Text = L"Prefere&nces...";
			// 
			// menuEditProperties
			// 
			this->menuEditProperties->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"menuEditProperties.Image")));
			this->menuEditProperties->ImageTransparentColor = System::Drawing::Color::Black;
			this->menuEditProperties->Name = L"menuEditProperties";
			this->menuEditProperties->Size = System::Drawing::Size(167, 22);
			this->menuEditProperties->Text = L"Pr&operties...";
			// 
			// menuHelp
			// 
			this->menuHelp->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(8) {this->menuHelpContents, 
				this->menuHelpIndex, this->menuHelpOnlineHelp, this->toolStripSeparator3, this->menuHelpLicenceInformation, this->menuHelpCheckForUpdates, 
				this->toolStripSeparator2, this->menuHelpAbout});
			this->menuHelp->Name = L"menuHelp";
			this->menuHelp->Size = System::Drawing::Size(44, 20);
			this->menuHelp->Text = L"&Help";
			this->menuHelp->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			// 
			// menuHelpContents
			// 
			this->menuHelpContents->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"menuHelpContents.Image")));
			this->menuHelpContents->Name = L"menuHelpContents";
			this->menuHelpContents->ShortcutKeys = System::Windows::Forms::Keys::F1;
			this->menuHelpContents->Size = System::Drawing::Size(181, 22);
			this->menuHelpContents->Text = L"&Contents";
			// 
			// menuHelpIndex
			// 
			this->menuHelpIndex->Name = L"menuHelpIndex";
			this->menuHelpIndex->Size = System::Drawing::Size(181, 22);
			this->menuHelpIndex->Text = L"&Index";
			// 
			// menuHelpOnlineHelp
			// 
			this->menuHelpOnlineHelp->Name = L"menuHelpOnlineHelp";
			this->menuHelpOnlineHelp->Size = System::Drawing::Size(181, 22);
			this->menuHelpOnlineHelp->Text = L"&Online Help";
			// 
			// toolStripSeparator3
			// 
			this->toolStripSeparator3->Name = L"toolStripSeparator3";
			this->toolStripSeparator3->Size = System::Drawing::Size(178, 6);
			// 
			// menuHelpLicenceInformation
			// 
			this->menuHelpLicenceInformation->Name = L"menuHelpLicenceInformation";
			this->menuHelpLicenceInformation->Size = System::Drawing::Size(181, 22);
			this->menuHelpLicenceInformation->Text = L"&Licence Information";
			// 
			// menuHelpCheckForUpdates
			// 
			this->menuHelpCheckForUpdates->Name = L"menuHelpCheckForUpdates";
			this->menuHelpCheckForUpdates->Size = System::Drawing::Size(181, 22);
			this->menuHelpCheckForUpdates->Text = L"Check for &Updates";
			// 
			// toolStripSeparator2
			// 
			this->toolStripSeparator2->Name = L"toolStripSeparator2";
			this->toolStripSeparator2->Size = System::Drawing::Size(178, 6);
			// 
			// menuHelpAbout
			// 
			this->menuHelpAbout->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"menuHelpAbout.Image")));
			this->menuHelpAbout->Name = L"menuHelpAbout";
			this->menuHelpAbout->Size = System::Drawing::Size(181, 22);
			this->menuHelpAbout->Text = L"&About MVCForms ...";
			// 
			// toolbar
			// 
			this->toolbar->GripStyle = System::Windows::Forms::ToolStripGripStyle::Hidden;
			this->toolbar->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(18) {this->buttonFileNew, this->buttonFileOpen, 
				this->buttonFileSave, this->buttonFilePrint, this->buttonSeparator0, this->buttonEditUndo, this->buttonEditRedo, this->buttonEditCut, 
				this->buttonEditCopy, this->buttonEditPaste, this->buttonEditDelete, this->buttonEditFind, this->buttonEditReplace, this->buttonEditRefresh, 
				this->buttonEditPreferences, this->buttonEditProperties, this->buttonSeparator1, this->buttonHelpContents});
			this->toolbar->Location = System::Drawing::Point(0, 24);
			this->toolbar->Name = L"toolbar";
			this->toolbar->Size = System::Drawing::Size(624, 25);
			this->toolbar->TabIndex = 119;
			this->toolbar->Text = L"toolStrip1";
			// 
			// buttonFileNew
			// 
			this->buttonFileNew->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->buttonFileNew->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"buttonFileNew.Image")));
			this->buttonFileNew->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->buttonFileNew->Name = L"buttonFileNew";
			this->buttonFileNew->Size = System::Drawing::Size(23, 22);
			this->buttonFileNew->Text = L"&New";
			// 
			// buttonFileOpen
			// 
			this->buttonFileOpen->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->buttonFileOpen->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"buttonFileOpen.Image")));
			this->buttonFileOpen->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->buttonFileOpen->Name = L"buttonFileOpen";
			this->buttonFileOpen->Size = System::Drawing::Size(23, 22);
			this->buttonFileOpen->Text = L"&Open";
			// 
			// buttonFileSave
			// 
			this->buttonFileSave->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->buttonFileSave->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"buttonFileSave.Image")));
			this->buttonFileSave->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->buttonFileSave->Name = L"buttonFileSave";
			this->buttonFileSave->Size = System::Drawing::Size(23, 22);
			this->buttonFileSave->Text = L"&Save";
			// 
			// buttonFilePrint
			// 
			this->buttonFilePrint->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->buttonFilePrint->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"buttonFilePrint.Image")));
			this->buttonFilePrint->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->buttonFilePrint->Name = L"buttonFilePrint";
			this->buttonFilePrint->Size = System::Drawing::Size(23, 22);
			this->buttonFilePrint->Text = L"Print";
			// 
			// buttonSeparator0
			// 
			this->buttonSeparator0->Name = L"buttonSeparator0";
			this->buttonSeparator0->Size = System::Drawing::Size(6, 25);
			// 
			// buttonEditUndo
			// 
			this->buttonEditUndo->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->buttonEditUndo->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"buttonEditUndo.Image")));
			this->buttonEditUndo->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->buttonEditUndo->Name = L"buttonEditUndo";
			this->buttonEditUndo->Size = System::Drawing::Size(23, 22);
			this->buttonEditUndo->Text = L"Undo";
			// 
			// buttonEditRedo
			// 
			this->buttonEditRedo->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->buttonEditRedo->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"buttonEditRedo.Image")));
			this->buttonEditRedo->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->buttonEditRedo->Name = L"buttonEditRedo";
			this->buttonEditRedo->Size = System::Drawing::Size(23, 22);
			this->buttonEditRedo->Text = L"Redo";
			// 
			// buttonEditCut
			// 
			this->buttonEditCut->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->buttonEditCut->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"buttonEditCut.Image")));
			this->buttonEditCut->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->buttonEditCut->Name = L"buttonEditCut";
			this->buttonEditCut->Size = System::Drawing::Size(23, 22);
			this->buttonEditCut->Text = L"Cut";
			// 
			// buttonEditCopy
			// 
			this->buttonEditCopy->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->buttonEditCopy->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"buttonEditCopy.Image")));
			this->buttonEditCopy->ImageTransparentColor = System::Drawing::Color::Black;
			this->buttonEditCopy->Name = L"buttonEditCopy";
			this->buttonEditCopy->Size = System::Drawing::Size(23, 22);
			this->buttonEditCopy->Text = L"Copy";
			// 
			// buttonEditPaste
			// 
			this->buttonEditPaste->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->buttonEditPaste->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"buttonEditPaste.Image")));
			this->buttonEditPaste->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->buttonEditPaste->Name = L"buttonEditPaste";
			this->buttonEditPaste->Size = System::Drawing::Size(23, 22);
			this->buttonEditPaste->Text = L"Paste";
			// 
			// buttonEditDelete
			// 
			this->buttonEditDelete->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->buttonEditDelete->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"buttonEditDelete.Image")));
			this->buttonEditDelete->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->buttonEditDelete->Name = L"buttonEditDelete";
			this->buttonEditDelete->Size = System::Drawing::Size(23, 22);
			this->buttonEditDelete->Text = L"Delete";
			// 
			// buttonEditFind
			// 
			this->buttonEditFind->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->buttonEditFind->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"buttonEditFind.Image")));
			this->buttonEditFind->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->buttonEditFind->Name = L"buttonEditFind";
			this->buttonEditFind->Size = System::Drawing::Size(23, 22);
			this->buttonEditFind->Text = L"Find";
			// 
			// buttonEditReplace
			// 
			this->buttonEditReplace->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->buttonEditReplace->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"buttonEditReplace.Image")));
			this->buttonEditReplace->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->buttonEditReplace->Name = L"buttonEditReplace";
			this->buttonEditReplace->Size = System::Drawing::Size(23, 22);
			this->buttonEditReplace->Text = L"Replace";
			// 
			// buttonEditRefresh
			// 
			this->buttonEditRefresh->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->buttonEditRefresh->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"buttonEditRefresh.Image")));
			this->buttonEditRefresh->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->buttonEditRefresh->Name = L"buttonEditRefresh";
			this->buttonEditRefresh->Size = System::Drawing::Size(23, 22);
			this->buttonEditRefresh->Text = L"Refresh";
			// 
			// buttonEditPreferences
			// 
			this->buttonEditPreferences->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->buttonEditPreferences->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"buttonEditPreferences.Image")));
			this->buttonEditPreferences->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->buttonEditPreferences->Name = L"buttonEditPreferences";
			this->buttonEditPreferences->Size = System::Drawing::Size(23, 22);
			this->buttonEditPreferences->Text = L"Preferences";
			// 
			// buttonEditProperties
			// 
			this->buttonEditProperties->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->buttonEditProperties->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"buttonEditProperties.Image")));
			this->buttonEditProperties->ImageTransparentColor = System::Drawing::Color::Black;
			this->buttonEditProperties->Name = L"buttonEditProperties";
			this->buttonEditProperties->Size = System::Drawing::Size(23, 22);
			this->buttonEditProperties->Text = L"P&roperties...";
			// 
			// buttonSeparator1
			// 
			this->buttonSeparator1->Name = L"buttonSeparator1";
			this->buttonSeparator1->Size = System::Drawing::Size(6, 25);
			// 
			// buttonHelpContents
			// 
			this->buttonHelpContents->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->buttonHelpContents->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"buttonHelpContents.Image")));
			this->buttonHelpContents->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->buttonHelpContents->Name = L"buttonHelpContents";
			this->buttonHelpContents->Size = System::Drawing::Size(23, 22);
			this->buttonHelpContents->Text = L"Contents";
			// 
			// statusBar
			// 
			this->statusBar->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(6) {this->StatusBarStatusMessage, 
				this->StatusBarErrorMessage, this->StatusBarProgressBar, this->StatusBarActionIcon, this->StatusBarDirtyMessage, this->StatusBarNetworkIndicator});
			this->statusBar->Location = System::Drawing::Point(0, 420);
			this->statusBar->Name = L"statusBar";
			this->statusBar->Size = System::Drawing::Size(624, 22);
			this->statusBar->TabIndex = 120;
			this->statusBar->Text = L"statusStrip1";
			// 
			// StatusBarStatusMessage
			// 
			this->StatusBarStatusMessage->ForeColor = System::Drawing::Color::Green;
			this->StatusBarStatusMessage->Name = L"StatusBarStatusMessage";
			this->StatusBarStatusMessage->Size = System::Drawing::Size(0, 17);
			this->StatusBarStatusMessage->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// StatusBarErrorMessage
			// 
			this->StatusBarErrorMessage->AutoToolTip = true;
			this->StatusBarErrorMessage->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
			this->StatusBarErrorMessage->ForeColor = System::Drawing::Color::Red;
			this->StatusBarErrorMessage->Name = L"StatusBarErrorMessage";
			this->StatusBarErrorMessage->Size = System::Drawing::Size(609, 17);
			this->StatusBarErrorMessage->Spring = true;
			this->StatusBarErrorMessage->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// StatusBarProgressBar
			// 
			this->StatusBarProgressBar->Alignment = System::Windows::Forms::ToolStripItemAlignment::Right;
			this->StatusBarProgressBar->Name = L"StatusBarProgressBar";
			this->StatusBarProgressBar->Size = System::Drawing::Size(100, 16);
			this->StatusBarProgressBar->Value = 10;
			this->StatusBarProgressBar->Visible = false;
			// 
			// StatusBarActionIcon
			// 
			this->StatusBarActionIcon->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->StatusBarActionIcon->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"StatusBarActionIcon.Image")));
			this->StatusBarActionIcon->ImageAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->StatusBarActionIcon->Name = L"StatusBarActionIcon";
			this->StatusBarActionIcon->Size = System::Drawing::Size(16, 17);
			this->StatusBarActionIcon->Visible = false;
			// 
			// StatusBarDirtyMessage
			// 
			this->StatusBarDirtyMessage->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->StatusBarDirtyMessage->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"StatusBarDirtyMessage.Image")));
			this->StatusBarDirtyMessage->ImageAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->StatusBarDirtyMessage->Name = L"StatusBarDirtyMessage";
			this->StatusBarDirtyMessage->Size = System::Drawing::Size(16, 17);
			this->StatusBarDirtyMessage->ToolTipText = L"Dirty";
			this->StatusBarDirtyMessage->Visible = false;
			// 
			// StatusBarNetworkIndicator
			// 
			this->StatusBarNetworkIndicator->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->StatusBarNetworkIndicator->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"StatusBarNetworkIndicator.Image")));
			this->StatusBarNetworkIndicator->ImageAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->StatusBarNetworkIndicator->ImageTransparentColor = System::Drawing::Color::Fuchsia;
			this->StatusBarNetworkIndicator->Name = L"StatusBarNetworkIndicator";
			this->StatusBarNetworkIndicator->Size = System::Drawing::Size(16, 17);
			this->StatusBarNetworkIndicator->ToolTipText = L"Network Connected";
			this->StatusBarNetworkIndicator->Visible = false;
			// 
			// chkStillAnotherBoolean
			// 
			this->chkStillAnotherBoolean->AutoSize = true;
			this->chkStillAnotherBoolean->CheckAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->chkStillAnotherBoolean->Location = System::Drawing::Point(363, 113);
			this->chkStillAnotherBoolean->Name = L"chkStillAnotherBoolean";
			this->chkStillAnotherBoolean->Size = System::Drawing::Size(65, 17);
			this->chkStillAnotherBoolean->TabIndex = 151;
			this->chkStillAnotherBoolean->Text = L"Boolean";
			this->chkStillAnotherBoolean->UseVisualStyleBackColor = true;
			// 
			// txtStillAnotherString
			// 
			this->txtStillAnotherString->Location = System::Drawing::Point(414, 87);
			this->txtStillAnotherString->Name = L"txtStillAnotherString";
			this->txtStillAnotherString->Size = System::Drawing::Size(100, 20);
			this->txtStillAnotherString->TabIndex = 150;
			// 
			// txtStillAnotherInt
			// 
			this->txtStillAnotherInt->Location = System::Drawing::Point(414, 61);
			this->txtStillAnotherInt->Name = L"txtStillAnotherInt";
			this->txtStillAnotherInt->Size = System::Drawing::Size(100, 20);
			this->txtStillAnotherInt->TabIndex = 149;
			// 
			// lblStillAnotherString
			// 
			this->lblStillAnotherString->AutoSize = true;
			this->lblStillAnotherString->Location = System::Drawing::Point(373, 90);
			this->lblStillAnotherString->Name = L"lblStillAnotherString";
			this->lblStillAnotherString->Size = System::Drawing::Size(34, 13);
			this->lblStillAnotherString->TabIndex = 148;
			this->lblStillAnotherString->Text = L"String";
			// 
			// lblStillAnotherInt
			// 
			this->lblStillAnotherInt->AutoSize = true;
			this->lblStillAnotherInt->Location = System::Drawing::Point(373, 64);
			this->lblStillAnotherInt->Name = L"lblStillAnotherInt";
			this->lblStillAnotherInt->Size = System::Drawing::Size(31, 13);
			this->lblStillAnotherInt->TabIndex = 147;
			this->lblStillAnotherInt->Text = L"Int32";
			// 
			// chkSomeOtherBoolean
			// 
			this->chkSomeOtherBoolean->AutoSize = true;
			this->chkSomeOtherBoolean->CheckAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->chkSomeOtherBoolean->Location = System::Drawing::Point(191, 113);
			this->chkSomeOtherBoolean->Name = L"chkSomeOtherBoolean";
			this->chkSomeOtherBoolean->Size = System::Drawing::Size(65, 17);
			this->chkSomeOtherBoolean->TabIndex = 146;
			this->chkSomeOtherBoolean->Text = L"Boolean";
			this->chkSomeOtherBoolean->UseVisualStyleBackColor = true;
			// 
			// txtSomeOtherString
			// 
			this->txtSomeOtherString->Location = System::Drawing::Point(242, 87);
			this->txtSomeOtherString->Name = L"txtSomeOtherString";
			this->txtSomeOtherString->Size = System::Drawing::Size(100, 20);
			this->txtSomeOtherString->TabIndex = 145;
			// 
			// txtSomeOtherInt
			// 
			this->txtSomeOtherInt->Location = System::Drawing::Point(242, 61);
			this->txtSomeOtherInt->Name = L"txtSomeOtherInt";
			this->txtSomeOtherInt->Size = System::Drawing::Size(100, 20);
			this->txtSomeOtherInt->TabIndex = 144;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(201, 90);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(34, 13);
			this->label3->TabIndex = 143;
			this->label3->Text = L"String";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(201, 64);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(31, 13);
			this->label4->TabIndex = 142;
			this->label4->Text = L"Int32";
			// 
			// cmdRun
			// 
			this->cmdRun->Location = System::Drawing::Point(532, 54);
			this->cmdRun->Name = L"cmdRun";
			this->cmdRun->Size = System::Drawing::Size(75, 23);
			this->cmdRun->TabIndex = 141;
			this->cmdRun->Text = L"Run";
			this->cmdRun->UseVisualStyleBackColor = true;
			// 
			// chkSomeBoolean
			// 
			this->chkSomeBoolean->AutoSize = true;
			this->chkSomeBoolean->CheckAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->chkSomeBoolean->Location = System::Drawing::Point(16, 113);
			this->chkSomeBoolean->Name = L"chkSomeBoolean";
			this->chkSomeBoolean->Size = System::Drawing::Size(65, 17);
			this->chkSomeBoolean->TabIndex = 140;
			this->chkSomeBoolean->Text = L"Boolean";
			this->chkSomeBoolean->UseVisualStyleBackColor = true;
			// 
			// txtSomeString
			// 
			this->txtSomeString->Location = System::Drawing::Point(67, 87);
			this->txtSomeString->Name = L"txtSomeString";
			this->txtSomeString->Size = System::Drawing::Size(100, 20);
			this->txtSomeString->TabIndex = 139;
			// 
			// txtSomeInt
			// 
			this->txtSomeInt->Location = System::Drawing::Point(67, 61);
			this->txtSomeInt->Name = L"txtSomeInt";
			this->txtSomeInt->Size = System::Drawing::Size(100, 20);
			this->txtSomeInt->TabIndex = 138;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(26, 90);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(34, 13);
			this->label2->TabIndex = 137;
			this->label2->Text = L"String";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(26, 64);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(31, 13);
			this->label5->TabIndex = 136;
			this->label5->Text = L"Int32";
			// 
			// FormView
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(624, 442);
			this->Controls->Add(this->chkStillAnotherBoolean);
			this->Controls->Add(this->txtStillAnotherString);
			this->Controls->Add(this->txtStillAnotherInt);
			this->Controls->Add(this->lblStillAnotherString);
			this->Controls->Add(this->lblStillAnotherInt);
			this->Controls->Add(this->chkSomeOtherBoolean);
			this->Controls->Add(this->txtSomeOtherString);
			this->Controls->Add(this->txtSomeOtherInt);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->cmdRun);
			this->Controls->Add(this->chkSomeBoolean);
			this->Controls->Add(this->txtSomeString);
			this->Controls->Add(this->txtSomeInt);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->statusBar);
			this->Controls->Add(this->toolbar);
			this->Controls->Add(this->menu);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->button1);
			this->Name = L"FormView";
			this->Text = L"FormView";
			this->menu->ResumeLayout(false);
			this->menu->PerformLayout();
			this->toolbar->ResumeLayout(false);
			this->toolbar->PerformLayout();
			this->statusBar->ResumeLayout(false);
			this->statusBar->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion


#pragma region IDisposable
		//~MVCView()
		//{
		//    Dispose(false);
		//}

		//public virtual void Dispose()
		//{
		//    // dispose of the managed and unmanaged resources
		//    Dispose(true);

		//    // tell the GC that the Finalize process no longer needs
		//    // to be run for this object.
		//    GC.SuppressFinalize(this);
		//}

		//protected virtual void Dispose(Boolean disposeManagedResources)
		//{
		//    // process only if mananged and unmanaged resources have
		//    // not been disposed of.
		//    if (!this.disposed)
		//    {
		//        //Resources not disposed
		//        if (disposeManagedResources)
		//        {
		//            // dispose managed resources
		//            //unsubscribe from model notifications
		//            if (this.PropertyChanged != null)
		//            {
		//                this.PropertyChanged -= PropertyChangedEventHandlerDelegate;
		//            }
		//        }
		//        // dispose unmanaged resources
		//        disposed = true;
		//    }
		//    else
		//    {
		//        //Resources already disposed
		//    }
		//}
#pragma endregion IDisposable

#pragma region INotifyPropertyChanged
	public:
		event PropertyChangedEventHandler^ PropertyChanged;
	protected:
		void OnPropertyChanged(String^ propertyName)
		{
			try
			{
				//if (this->PropertyChanged != nullptr)
				//{
					this->PropertyChanged(this, gcnew PropertyChangedEventArgs(propertyName));
				//}
			}
			catch (Exception^ ex)
			{
				ViewModel->ErrorMessage = ex->Message;
				Log->Write(ex, MethodBase->GetCurrentMethod(), EventLogEntryType::Error);

				throw;
			}
		}
#pragma endregion INotifyPropertyChanged

#pragma region PropertyChangedEventHandlerDelegates
		/// <summary>
		/// Note: model property changes update UI manually.
		/// Note: handle settings property changes manually.
		/// Note: because settings properties are a subset of the model 
		///  (every settings property should be in the model, 
		///  but not every model property is persisted to settings)
		///  it is decided that for now the settigns handler will 
		///  invoke the model handler as well.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void PropertyChangedEventHandlerDelegate(Object sender, PropertyChangedEventArgs^ e)
		{
			try
			{
#pragma region Model
				if (e->PropertyName == L"IsChanged")
				{
					//ConsoleApplication.defaultOutputDelegate(String::Format("{0}", e.PropertyName));
					ApplySettings();
				}
				//Status Bar
				else if (e->PropertyName == L"ActionIconIsVisible")
				{
					StatusBarActionIcon->Visible = (ViewModel->ActionIconIsVisible);
				}
				else if (e->PropertyName == L"ActionIconImage")
				{
					StatusBarActionIcon->Image = (ViewModel != nullptr ? ViewModel->ActionIconImage : nullptr);
				}
				else if (e->PropertyName == L"StatusMessage")
				{
					//replace default action by setting control property
					//skip status message updates after Viewmodel is null
					StatusBarStatusMessage->Text = (ViewModel != nullptr ? ViewModel->StatusMessage : nullptr);
					//e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

					//ConsoleApplication.defaultOutputDelegate(String::Format("{0}", StatusMessage));
				}
				else if (e->PropertyName == L"ErrorMessage")
				{
					//replace default action by setting control property
					//skip status message updates after Viewmodel is null
					StatusBarErrorMessage->Text = (ViewModel != nullptr ? ViewModel->ErrorMessage : nullptr);
					//e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

					//ConsoleApplication.defaultOutputDelegate(String::Format("{0}", ErrorMessage));
				}
				else if (e->PropertyName == L"CustomMessage")
				{
					//replace default action by setting control property
					StatusBarCustomMessage->Text = ViewModel->CustomMessage;
					//e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

					//ConsoleApplication.defaultOutputDelegate(String::Format("{0}", ErrorMessage));
				}
				else if (e->PropertyName == L"ErrorMessageToolTipText")
				{
					StatusBarErrorMessage->ToolTipText = ViewModel->ErrorMessageToolTipText;
				}
				else if (e->PropertyName == L"ProgressBarValue")
				{
					StatusBarProgressBar->Value = ViewModel->ProgressBarValue;
				}
				else if (e->PropertyName == L"ProgressBarMaximum")
				{
					StatusBarProgressBar->Maximum = ViewModel->ProgressBarMaximum;
				}
				else if (e->PropertyName == L"ProgressBarMinimum")
				{
					StatusBarProgressBar->Minimum = ViewModel->ProgressBarMinimum;
				}
				else if (e->PropertyName == L"ProgressBarStep")
				{
					StatusBarProgressBar->Step = ViewModel->ProgressBarStep;
				}
				else if (e->PropertyName == L"ProgressBarIsMarquee")
				{
					StatusBarProgressBar->Style = (ViewModel->ProgressBarIsMarquee ? ProgressBarStyle::Marquee : ProgressBarStyle::Blocks);
				}
				else if (e->PropertyName == L"ProgressBarIsVisible")
				{
					StatusBarProgressBar->Visible = (ViewModel->ProgressBarIsVisible);
				}
				else if (e->PropertyName == L"DirtyIconIsVisible")
				{
					StatusBarDirtyMessage->Visible = (ViewModel->DirtyIconIsVisible);
				}
				else if (e->PropertyName == L"DirtyIconImage")
				{
					StatusBarDirtyMessage->Image = ViewModel->DirtyIconImage;
				}
				//use if properties cannot be databound
				//else if (e.PropertyName == "SomeInt")
				//{
				//    //SettingsController<MVCSettings>.Settings.
				//    ModelController<MVCModel>.Model.IsChanged = true;
				//}
				//else if (e.PropertyName == "SomeBoolean")
				//{
				//    //SettingsController<MVCSettings>.Settings.
				//    ModelController<MVCModel>.Model.IsChanged = true;
				//}
				//else if (e.PropertyName == "SomeString")
				//{
				//    //SettingsController<MVCSettings>.Settings.
				//    ModelController<MVCModel>.Model.IsChanged = true;
				//}
				//else if (e.PropertyName == "SomeOtherBoolean")
				//{
				//    ConsoleApplication.defaultOutputDelegate(String::Format("SomeOtherBoolean: {0}", ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean));
				//}
				//else if (e.PropertyName == "SomeOtherString")
				//{
				//    ConsoleApplication.defaultOutputDelegate(String::Format("SomeOtherString: {0}", ModelController<MVCModel>.Model.SomeComponent.SomeOtherString));
				//}
				//else if (e.PropertyName == "SomeComponent")
				//{
				//    ConsoleApplication.defaultOutputDelegate(String::Format("SomeComponent: {0},{1},{2}", ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt, ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean, ModelController<MVCModel>.Model.SomeComponent.SomeOtherString));
				//}
				//else if (e.PropertyName == "StillAnotherInt")
				//{
				//    ConsoleApplication.defaultOutputDelegate(String::Format("StillAnotherInt: {0}", ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt));
				//}
				//else if (e.PropertyName == "StillAnotherBoolean")
				//{
				//    ConsoleApplication.defaultOutputDelegate(String::Format("StillAnotherBoolean: {0}", ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean));
				//}
				//else if (e.PropertyName == "StillAnotherString")
				//{
				//    ConsoleApplication.defaultOutputDelegate(String::Format("StillAnotherString: {0}", ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString));
				//}
				//else if (e.PropertyName == "StillAnotherComponent")
				//{
				//    ConsoleApplication.defaultOutputDelegate(String::Format("StillAnotherComponent: {0},{1},{2}", ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt, ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean, ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString));
				//}
				else
				{
#if defined(DEBUG_MODEL_PROPERTYCHANGED)
						ConsoleApplication::defaultOutputDelegate(StringHelper::formatSimple(L"e.PropertyName: {0}", e->PropertyName));
#endif
				}
#pragma endregion Model

#pragma region Settings
				if (e->PropertyName == L"Dirty")
				{
					//apply settings that don't have databindings
					ViewModel->DirtyIconIsVisible = (SettingsController<MVCSettings^>::Settings->Dirty);
				}
				else
				{
#if defined(DEBUG_SETTINGS_PROPERTYCHANGED)
					ConsoleApplication::defaultOutputDelegate(StringHelper::formatSimple(L"e.PropertyName: {0}", e->PropertyName));
#endif
				}
#pragma endregion Settings
			}
			catch (Exception^ ex)
			{
				Log->Write(ex, MethodBase->GetCurrentMethod(), EventLogEntryType::Error);
			}
		}

#pragma endregion PropertyChangedEventHandlerDelegates

#pragma region Properties
			private:
				String^ _ViewName;
//C# TO C++ CONVERTER TODO TASK: Local functions are not converted by C# to C++ Converter:
			public:
				String^ getViewName()
				{
					return _ViewName;
				}
				void setViewName(String^ value)
				{
					_ViewName = value;
					OnPropertyChanged(L"ViewName");
				}
#pragma endregion Properties

#pragma region Events
#pragma region Form Events
		/// <summary>
		/// Process Form key presses.
		/// </summary>
		/// <param name="msg"></param>
		/// <param name="keyData"></param>
		/// <returns></returns>
	protected:
		virtual Boolean ProcessCmdKey(Message^ msg, Keys^ keyData) override
		{
			Boolean returnValue = false;
			try
			{
				// Implement the Escape / Cancel keystroke
				if (keyData == Keys::Cancel || keyData == Keys::Escape)
				{
					//if a long-running cancellable-action has registered 
					//an escapable-event, trigger it
					InvokeActionCancel();

					// This keystroke was handled, 
					//don't pass to the control with the focus
					returnValue = true;
				}
				else
				{
					returnValue = __super::ProcessCmdKey(msg, keyData);
				}

			}
			catch (Exception^ ex)
			{
				Log->Write(ex, MethodBase->GetCurrentMethod(), EventLogEntryType::Error);
			}
			return returnValue;
		}

	private:
		void View_Load(Object^ sender, EventArgs^  e)
		{
			try
			{
				ViewModel->StatusMessage = String::Format(L"{0} starting...", ViewName);

				ViewModel->StatusMessage = String::Format(L"{0} started.", ViewName);

				_Run();
			}
			catch (Exception^ ex)
			{
				ViewModel->ErrorMessage = ex->Message;
				ViewModel->StatusMessage = L"";

				Log->Write(ex, MethodBase->GetCurrentMethod(), EventLogEntryType::Error);
			}
		}

		void View_FormClosing(Object^ sender, FormClosingEventArgs^  e)
		{
			try
			{
				ViewModel->StatusMessage = String::Format(L"{0} completing...", ViewName);

				DisposeSettings();

				ViewModel->StatusMessage = String::Format(L"{0} completed.", ViewName);
			}
			catch (Exception^ ex)
			{
//C# TO C++ CONVERTER TODO TASK: There is no C++ equivalent to 'ToString':
				ViewModel->ErrorMessage = ex->Message->ToString();
				ViewModel->StatusMessage = L"";

				Log->Write(ex, MethodBase->GetCurrentMethod(), EventLogEntryType::Error);
			}
//C# TO C++ CONVERTER TODO TASK: There is no C++ equivalent to the exception 'finally' clause:
			finally
			{
				ViewModel = nullptr;
			}
		}
#pragma endregion Form Events

#pragma region Menu Events
		void menuFileNew_Click(Object^ sender, EventArgs^  e)
		{
			ViewModel->FileNew();
		}

		void menuFileOpen_Click(Object^ sender, EventArgs^  e)
		{
			ViewModel->FileOpen();
		}

		void menuFileSave_Click(Object^ sender, EventArgs^  e)
		{
			ViewModel->FileSave();
		}

		void menuFileSaveAs_Click(Object^ sender, EventArgs^  e)
		{
			ViewModel->FileSaveAs();
		}

		void menuFilePrint_Click(Object^ sender, EventArgs^  e)
		{
			ViewModel->FilePrint();
		}

		void menuFileExit_Click(Object^ sender, EventArgs^  e)
		{
			ViewModel->FileExit();
		}

		void menuEditUndo_Click(Object^ sender, EventArgs^  e)
		{
			ViewModel->EditUndo();
		}

		void menuEditRedo_Click(Object^ sender, EventArgs^  e)
		{
			ViewModel->EditRedo();
		}

		void menuEditSelectAll_Click(Object^ sender, EventArgs^  e)
		{
			ViewModel->EditSelectAll();
		}

		void menuEditCut_Click(Object^ sender, EventArgs^  e)
		{
			ViewModel->EditCut();
		}

		void menuEditCopy_Click(Object^ sender, EventArgs^  e)
		{
			ViewModel->EditCopy();
		}

		void menuEditPaste_Click(Object^ sender, EventArgs^  e)
		{
			ViewModel->EditPaste();
		}

		void menuEditDelete_Click(Object^ sender, EventArgs^  e)
		{
			ViewModel->EditDelete();
		}

		void menuEditFind_Click(Object^ sender, EventArgs^  e)
		{
			ViewModel->EditFind();
		}

		void menuEditReplace_Click(Object^ sender, EventArgs^  e)
		{
			ViewModel->EditReplace();
		}

		void menuEditRefresh_Click(Object^ sender, EventArgs^  e)
		{
			ViewModel->EditRefresh();
		}

		void menuEditPreferences_Click(Object^ sender, EventArgs^  e)
		{
			ViewModel->EditPreferences();
		}

		void menuEditProperties_Click(Object^ sender, EventArgs^  e)
		{
			ViewModel->EditProperties();
		}

		void menuHelpContents_Click(Object^ sender, EventArgs^  e)
		{
			ViewModel->HelpContents();
		}

		void menuHelpIndex_Click(Object^ sender, EventArgs^  e)
		{
			ViewModel->HelpIndex();
		}

		void menuHelpOnlineHelp_Click(Object^ sender, EventArgs^  e)
		{
			ViewModel->HelpOnHelp();
		}

		void menuHelpLicenceInformation_Click(Object^ sender, EventArgs^  e)
		{
			ViewModel->HelpLicenceInformation();
		}

		void menuHelpCheckForUpdates_Click(Object^ sender, EventArgs^  e)
		{
			ViewModel->HelpCheckForUpdates();
		}

		void menuHelpAbout_Click(Object^ sender, EventArgs^  e)
		{
			ViewModel->HelpAbout<AssemblyInfo^>();
		}
#pragma endregion Menu Events

#pragma region Control Events
		void cmdRun_Click(Object^ sender, EventArgs^  e)
		{
			ViewModel->DoSomething();
			//ViewModel.CustomMessage = "blah";//done in DoSomething
		}

		System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
			Class1^ c1 = gcnew Class1();

			MessageBox::Show(c1->Hello(textBox1->Text));

			c1 = nullptr;
		 }
#pragma endregion Control Events
#pragma endregion Events

#pragma region Methods
#pragma region FormAppBase
	protected:
		void InitViewModel()
		{
			Dictionary<String^, Bitmap^>^ d;
			try
			{
				//tell controller how model should notify view about non-persisted properties AND including model properties that may be part of settings
				ModelController<MVCModel^>::DefaultHandler = PropertyChangedEventHandlerDelegate;

				//tell controller how settings should notify view about persisted properties
				SettingsController<MVCSettings^>::DefaultHandler = PropertyChangedEventHandlerDelegate;

				InitModelAndSettings();

				FileDialogInfo^ settingsFileDialogInfo = new FileDialogInfo(SettingsController<MVCSettings^>::FILE_NEW, nullptr, nullptr, MVCSettings::FileTypeExtension, MVCSettings::FileTypeDescription, MVCSettings::FileTypeName, new String^[] {L"XML files (*.xml)|*.xml", L"All files (*.*)|*.*"}, false, nullptr, PathExtensions::WithTrailingSeparator(Environment::GetFolderPath(Environment::SpecialFolder::Personal)));

				//set dialog caption
				settingsFileDialogInfo->Title = this->Text;

				d = Dictionary<String^, Bitmap^>();
				d->Add(L"New", MVCFormsCpp::Properties->Resources->New);
				d->Add(L"Open", MVCFormsCpp::Properties->Resources->Open);
				d->Add(L"Save", MVCFormsCpp::Properties->Resources->Save);
				d->Add(L"Print", MVCFormsCpp::Properties->Resources->Print);
				d->Add(L"Undo", MVCFormsCpp::Properties->Resources->Undo);
				d->Add(L"Redo", MVCFormsCpp::Properties->Resources->Redo);
				d->Add(L"Cut", MVCFormsCpp::Properties->Resources->Cut);
				d->Add(L"Copy", MVCFormsCpp::Properties->Resources->Copy);
				d->Add(L"Paste", MVCFormsCpp::Properties->Resources->Paste);
				d->Add(L"Delete", MVCFormsCpp::Properties->Resources->Delete);
				d->Add(L"Find", MVCFormsCpp::Properties->Resources->Find);
				d->Add(L"Replace", MVCFormsCpp::Properties->Resources->Replace);
				d->Add(L"Refresh", MVCFormsCpp::Properties->Resources->Reload);
				d->Add(L"Preferences", MVCFormsCpp::Properties->Resources->Preferences);
				d->Add(L"Properties", MVCFormsCpp::Properties->Resources->Properties);
				d->Add(L"Contents", MVCFormsCpp::Properties->Resources->Contents);
				d->Add(L"About", MVCFormsCpp::Properties->Resources->About);

				//class to handle standard behaviors
				MVCViewModel tempVar(this->PropertyChangedEventHandlerDelegate, d, settingsFileDialogInfo);
				ViewModelController<Bitmap^, MVCViewModel*>::New(ViewName, &tempVar);

				//select a viewmodel by view name
				ViewModel = ViewModelController<Bitmap^, MVCViewModel*>::ViewModel[ViewName];

				BindFormUi();

				//Init config parameters
				if (!LoadParameters())
				{
//C# TO C++ CONVERTER TODO TASK: A 'delete settingsFileDialogInfo' statement was not added since settingsFileDialogInfo was passed to a method or constructor. Handle memory management manually.
					throw std::runtime_error(String^::Format(L"Unable to load config file parameter(s)."));
				}

				//DEBUG:filename coming in is being converted/passed as DOS 8.3 format equivalent
				//Load
				if ((SettingsController<MVCSettings^>::FilePath == nullptr) || (SettingsController<MVCSettings^>::Filename == SettingsController<MVCSettings^>::FILE_NEW))
				{
					//NEW
					ViewModel::FileNew();
				}
				else
				{
					//OPEN
					ViewModel::FileOpen(false);
				}

#if defined(debug)
			//debug view
			menuEditProperties_Click(sender, e);
#endif

				//Display dirty state
				ModelController<MVCModel^>::Model->Refresh();

//C# TO C++ CONVERTER TODO TASK: A 'delete settingsFileDialogInfo' statement was not added since settingsFileDialogInfo was passed to a method or constructor. Handle memory management manually.
			}
			catch (Exception^ ex)
			{
				if (ViewModel != nullptr)
				{
					ViewModel->ErrorMessage = ex->Message;
				}
				Log->Write(ex, MethodBase->GetCurrentMethod(), EventLogEntryType::Error);
			}
		}

		void InitModelAndSettings()
		{
			//create Settings before first use by Model
			if (SettingsController<MVCSettings^>::Settings == nullptr)
			{
				SettingsController<MVCSettings^>::New();
			}
			//Model properties rely on Settings, so don't call Refresh before this is run.
			if (ModelController<MVCModel^>::Model == nullptr)
			{
				ModelController<MVCModel^>::New();
			}
		}

		void DisposeSettings()
		{
			//save user and application settings 
			Properties->Settings->Default.Save();

			if (SettingsController<MVCSettings^>::Settings->Dirty)
			{
				//prompt before saving
				DialogResult^ dialogResult = MessageBox::Show(L"Save changes?", this->Text, MessageBoxButtons::YesNo, MessageBoxIcon::Question);
				switch (dialogResult)
				{
					case DialogResult::Yes:
					{
							//SAVE
							ViewModel::FileSave();

							break;
					}
					case DialogResult::No:
					{
							break;
					}
					default:
					{
							throw InvalidEnumArgumentException();
					}
				}
			}

			//unsubscribe from model notifications
			ModelController<MVCModel^>::Model->PropertyChanged -= PropertyChangedEventHandlerDelegate;
		}

		void _Run()
		{
			//MessageBox.Show("running", "MVC", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
		}
#pragma endregion FormAppBase


#pragma region Utility
		/// <summary>
		/// Bind static Model controls to Model Controller
		/// </summary>
	private:
		void BindFormUi()
		{
			try
			{
				//Form

				//Controls
			}
			catch (Exception^ ex)
			{
				Log->Write(ex, MethodBase->GetCurrentMethod(), EventLogEntryType::Error);
				throw;
			}
		}

		/// <summary>
		/// Bind Model controls to Model Controller
		/// </summary>
		void BindModelUi()
		{
			try
			{
				BindField<TextBox*, MVCModel^>(txtSomeInt, ModelController<MVCModel^>::Model, L"SomeInt");
				BindField<TextBox*, MVCModel^>(txtSomeString, ModelController<MVCModel^>::Model, L"SomeString");
				BindField<CheckBox*, MVCModel^>(chkSomeBoolean, ModelController<MVCModel^>::Model, L"SomeBoolean", L"Checked");

				BindField<TextBox*, MVCModel^>(txtSomeOtherInt, ModelController<MVCModel^>::Model, L"SomeComponent.SomeOtherInt");
				BindField<TextBox*, MVCModel^>(txtSomeOtherString, ModelController<MVCModel^>::Model, L"SomeComponent.SomeOtherString");
				BindField<CheckBox*, MVCModel^>(chkSomeOtherBoolean, ModelController<MVCModel^>::Model, L"SomeComponent.SomeOtherBoolean", L"Checked");

				BindField<TextBox*, MVCModel^>(txtStillAnotherInt, ModelController<MVCModel^>::Model, L"StillAnotherComponent.StillAnotherInt");
				BindField<TextBox*, MVCModel^>(txtStillAnotherString, ModelController<MVCModel^>::Model, L"StillAnotherComponent.StillAnotherString");
				BindField<CheckBox*, MVCModel^>(chkStillAnotherBoolean, ModelController<MVCModel^>::Model, L"StillAnotherComponent.StillAnotherBoolean", L"Checked");
			}
			catch (Exception^ ex)
			{
				Log->Write(ex, MethodBase->GetCurrentMethod(), EventLogEntryType::Error);
				throw;
			}
		}

		template<typename TControl, typename TModel>
		void BindField(TControl fieldControl, TModel model, String^ modelPropertyName, String^ controlPropertyName, Boolean formattingEnabled, DataSourceUpdateMode^ dataSourceUpdateMode, Boolean reBind)
		{
			static_assert(std::is_base_of<Control, TControl>::value, L"TControl must inherit from Control");

			try
			{
				fieldControl->DataBindings->Clear();
				if (reBind)
				{
					fieldControl->DataBindings->Add(controlPropertyName, model, modelPropertyName, formattingEnabled, dataSourceUpdateMode);
				}
			}
			catch (Exception^ ex)
			{
				Log->Write(ex, MethodBase->GetCurrentMethod(), EventLogEntryType::Error);
			}
		}

		template<typename TControl, typename TModel>
		void BindField(TControl fieldControl, TModel model, String^ modelPropertyName, String^ controlPropertyName)
		{
			static_assert(std::is_base_of<Control, TControl>::value, L"TControl must inherit from Control");

			Boolean formattingEnabled = false;
			DataSourceUpdateMode^ dataSourceUpdateMode = DataSourceUpdateMode::OnPropertyChanged;
			Boolean reBind = true;

			BindField<TControl, TModel>(fieldControl, model, modelPropertyName, controlPropertyName, formattingEnabled, dataSourceUpdateMode, reBind);
		}

		template<typename TControl, typename TModel>
		void BindField(TControl fieldControl, TModel model, String^ modelPropertyName)
		{
			static_assert(std::is_base_of<Control, TControl>::value, L"TControl must inherit from Control");

			String^ controlPropertyName = L"Text";
			Boolean formattingEnabled = false;
			DataSourceUpdateMode^ dataSourceUpdateMode = DataSourceUpdateMode::OnPropertyChanged;
			Boolean reBind = true;

			BindField<TControl, TModel>(fieldControl, model, modelPropertyName, controlPropertyName, formattingEnabled, dataSourceUpdateMode, reBind);
		}

		/// <summary>
		/// Apply Settings to viewer.
		/// </summary>
		void ApplySettings()
		{
			try
			{
				_ValueChangedProgrammatically = true;

				//apply settings that have databindings
				BindModelUi();

				//apply settings that shouldn't use databindings

				//apply settings that can't use databindings
				Text = FileSystem::getFileName(SettingsController<MVCSettings^>::Filename) + L" - " + ViewName;

				//apply settings that don't have databindings
				ViewModel->DirtyIconIsVisible = (SettingsController<MVCSettings^>::Settings->Dirty);

				_ValueChangedProgrammatically = false;
			}
			catch (Exception^ ex)
			{
				Log->Write(ex, MethodBase->GetCurrentMethod(), EventLogEntryType::Error);
				throw;
			}
		}

		/// <summary>
		/// Set function button and menu to enable value, and cancel button to opposite.
		/// For now, do only disabling here and leave enabling based on biz logic 
		///  to be triggered by refresh?
		/// </summary>
		/// <param name="functionButton"></param>
		/// <param name="functionMenu"></param>
		/// <param name="cancelButton"></param>
		/// <param name="enable"></param>
		void SetFunctionControlsEnable(Button^ functionButton, ToolStripButton^ functionToolbarButton, ToolStripMenuItem^ functionMenu, Button^ cancelButton, Boolean enable)
		{
			try
			{
				//stand-alone button
				if (functionButton != nullptr)
				{
					functionButton->Enabled = enable;
				}

				//toolbar button
				if (functionToolbarButton != nullptr)
				{
					functionToolbarButton->Enabled = enable;
				}

				//menu item
				if (functionMenu != nullptr)
				{
					functionMenu->Enabled = enable;
				}

				//stand-alone cancel button
				if (cancelButton != nullptr)
				{
					cancelButton->Enabled = !enable;
				}
			}
			catch (Exception^ ex)
			{
				Log->Write(ex, MethodBase->GetCurrentMethod(), EventLogEntryType::Error);
			}
		}

		/// <summary>
		/// Invoke any delegate that has been registered 
		///  to cancel a long-running background process.
		/// </summary>
		void InvokeActionCancel()
		{
			try
			{
				//execute cancellation hook
				if (cancelDelegate != nullptr)
				{
					cancelDelegate();
				}
			}
			catch (Exception^ ex)
			{
				Log->Write(ex, MethodBase->GetCurrentMethod(), EventLogEntryType::Error);
			}
		}

		/// <summary>
		/// Load from app config; override with command line if present
		/// </summary>
		/// <returns></returns>
		Boolean LoadParameters()
		{
			Boolean returnValue = false;
#if defined(USE_CONFIG_FILEPATH)
			String^ _settingsFilename = L"";
#endif

			try
			{
				if ((Program::Filename != nullptr) && (Program::Filename != SettingsController<MVCSettings^>::FILE_NEW))
				{
					//got filename from command line
					//DEBUG:filename coming in is being converted/passed as DOS 8.3 format equivalent
					if (!RegistryAccess::ValidateFileAssociation(Application::ExecutablePath, L"." + MVCSettings::FileTypeExtension))
					{
						throw ApplicationException(String::Format(L"Settings filename not associated: '{0}'.\nCheck filename on command line.", Program::Filename));
					}
					//it passed; use value from command line
				}
				else
				{
#if defined(USE_CONFIG_FILEPATH)
					//get filename from app.config
					if (!Configuration::ReadString(L"SettingsFilename", _settingsFilename))
					{
						throw ApplicationException(String::Format(L"Unable to load SettingsFilename: {0}", L"SettingsFilename"));
					}
					if ((_settingsFilename == L"") || (_settingsFilename == SettingsController<MVCSettings^>::FILE_NEW))
					{
						throw ApplicationException(String::Format(L"Settings filename not set: '{0}'.\nCheck SettingsFilename in app config file.", _settingsFilename));
					}
					//use with the supplied path
					SettingsController<MVCSettings^>::Filename = _settingsFilename;

					if (FileSystem::getDirectoryName(_settingsFilename) == L"")
					{
						//supply default path if missing
						SettingsController<MVCSettings^>::Pathname = Environment::GetFolderPath(Environment::SpecialFolder::Personal)->WithTrailingSeparator();
					}
#endif
				}

				returnValue = true;
			}
			catch (Exception^ ex)
			{
				Log->Write(ex, MethodBase->GetCurrentMethod(), EventLogEntryType::Error);
				//throw;
			}
			return returnValue;
		}

		void BindSizeAndLocation()
		{
			//Note:Size must be done after InitializeComponent(); do Location this way as well.--SJS
			System::Windows::Forms::Binding tempVar(L"Location", MVCFormsCpp::Properties->Settings->Default, L"Location", true, System::Windows::Forms::DataSourceUpdateMode::OnPropertyChanged);
			this->DataBindings->Add(&tempVar);
			System::Windows::Forms::Binding tempVar2(L"ClientSize", MVCFormsCpp::Properties->Settings->Default, L"Size", true, System::Windows::Forms::DataSourceUpdateMode::OnPropertyChanged);
			this->DataBindings->Add(&tempVar2);
			this->ClientSize = MVCFormsCpp::Properties->Settings->Default.Size;
			this->Location = MVCFormsCpp::Properties->Settings->Default.Location;
		}
#pragma endregion Utility
#pragma endregion Methods
};
}

