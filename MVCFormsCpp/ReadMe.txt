========================================================================
    APPLICATION : MVCFormsCpp Project Overview
========================================================================

AppWizard has created this MVCFormsCpp Application for you.  

This file contains a summary of what you will find in each of the files that
make up your MVCFormsCpp application.

MVCFormsCpp.vcproj
    This is the main project file for VC++ projects generated using an Application Wizard. 
    It contains information about the version of Visual C++ that generated the file, and 
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

MVCFormsCpp.cpp
    This is the main application source file.
    Contains the code to display the form.

FormView.h
    Contains the implementation of your form class and InitializeComponent() function.

AssemblyInfo.cpp
    Contains custom attributes for modifying assembly metadata.

/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named MVCFormsCpp.pch and a precompiled types file named StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
