﻿p// Program.cpp : main project file.

#include "stdafx.h"
#include "Program.h"
#include "Globals.h"

namespace MVCFormsCpp
{
		#pragma region Declarations
		#pragma endregion Declarations

		#pragma region Constructors
		Program::~Program()
		{
		}

		Program::Program()
		{
			try
			{
				//subscribe to notifications
				//this->PropertyChanged += gcnew PropertyChangedEventHandler(this, this->PropertyChangedEventHandlerDelegate);//C3350: del ctor expects 1 param; acts a little like an extension-method, in that it seems to swallow an assumed 'this' for the 'Object^ sender' param
				this->PropertyChanged += gcnew PropertyChangedEventHandler( this->PropertyChangedEventHandlerDelegate);//OK
				//this->PropertyChanged += gcnew PropertyChangedEventHandler(Program::PropertyChangedEventHandlerDelegate);
			}
			catch (Exception^ ex)
			{
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);
			}
		}
		#pragma endregion Constructors

		#pragma region INotifyPropertyChanged
		void Program::OnPropertyChanged(String^ propertyName)
		{
			try
			{
				programInstance->PropertyChanged(programInstance, gcnew PropertyChangedEventArgs(propertyName));
			}
			catch (Exception^ ex)
			{
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);

				throw;
			}
		}
		#pragma endregion INotifyPropertyChanged

		#pragma region PropertyChangedEventHandlerDelegate
		/// <summary>
		/// Note: property changes update UI manually.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void Program::PropertyChangedEventHandlerDelegate(Object^ sender, PropertyChangedEventArgs^ e)
		{
			try
			{
				if (e->PropertyName == L"Filename")
				{
					ConsoleApplication::defaultOutputDelegate(String::Format(L"{0}", Globals::getFilename()));
				}
			}
			catch (Exception^ ex)
			{
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);
			}
		}
		#pragma endregion PropertyChangedEventHandlerDelegate

		#pragma region Properties
		#pragma endregion Properties

		#pragma region Methods
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		/// <param name="args"></param>
		/// <returns>Int32</returns>
//C# TO C++ CONVERTER NOTE: The following .NET attribute has no direct equivalent in C++:
//ORIGINAL LINE: [STAThread] static Int32 Main(String[] args)
		//[STAThreadAttribute]
		Int32 Program::Main(array<String^>^ args)
		{
			//default to fail code
			Int32 returnValue = -1;
			String^ name = "";
			Class1^ c1 = gcnew Class1();

			try
			{

				//begin temp code
				Console::Write("enter name>");
				name = Console::ReadLine();

				Console::WriteLine(c1->Hello(name));

				c1 = nullptr;

				Console::Write("Press any key to end");
				Console::ReadLine();

				//end temp code

				programInstance =  gcnew Program();

				//define default output delegate
				ConsoleApplication::defaultOutputDelegate = ConsoleApplication::messageBoxWrapperOutputDelegate;

				//subscribe to notifications
				programInstance->PropertyChanged += gcnew PropertyChangedEventHandler(PropertyChangedEventHandlerDelegate);
				//Program::PropertyChangedStatic += programInstance->PropertyChanged;

				//load, parse, run switches
				DoSwitches(args);

				// Enabling Windows XP visual effects before any controls are created
				Application::EnableVisualStyles();
				Application::SetCompatibleTextRenderingDefault(false); 

				// Create the main window and run it
				Application::Run(gcnew FormView());
				return 0;
			}
			catch (Exception^ ex)
			{
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);
				Console::WriteLine(String::Format(L"{0} did NOT complete: '{1}'", Globals::APP_NAME, ex->Message));
			}
//C# TO C++ CONVERTER TODO TASK: There is no C++ equivalent to the exception 'finally' clause:
			//finally
			//{
				#if defined(debug)
				Console.Write(L"Press ENTER to continue> ");
				#endif
				Console::ReadLine();

				programInstance = nullptr;
			//}
			return returnValue;
		}

		#pragma region ConsoleAppBase
		/// <summary>
		/// Note: switches are processed before Model or Settings are accessed.
		/// </summary>
		/// <param name="args"></param>
		/*static*/ void Program::DoSwitches(array<String^>^ args)
		{
			array<CommandLineSwitch^>^ sw = 
			{
				gcnew CommandLineSwitch(L"t", L"t tests switch.", true, gcnew ConsoleApplication::TCommandLineSwitchDelegate(t)), 
				gcnew CommandLineSwitch(L"f", L"f filename; overrides app.config", true, gcnew ConsoleApplication::TCommandLineSwitchDelegate(f)),
			};
			
			//define supported switches
			// -t -f:"filename" -h
			ConsoleApplication::DoCommandLineSwitches
			(
				args,  
				sw
			);
			//Note: switches are processed before Model or Settings are accessed.
		}

		//Note:model, Settings init done in viewmodel (after default handler set)
		#pragma endregion ConsoleAppBase

		#pragma region CommandLineSwitch Action Delegates
		/// <summary>
		/// Instance of an action conforming to delegate Action(Of TSettings), where TSettings is String.
		/// Command 't' tests the use of parameters.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="outputDelegate"></param>
		/*static */void Program::t(String^ value, ConsoleApplication::TOutputDelegate^ outputDelegate)
		{
			try
			{
				outputDelegate(String::Format(L"t:\t{0}", value));
			}
			catch (Exception^ ex)
			{
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);
				throw;
			}
		}

		/// <summary>
		/// Validate and set selected settings.
		/// Instance of an action conforming to delegate Action<T>, where T is String.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="outputDelegate"></param>
		/*static */void Program::f(String^ value, ConsoleApplication::TOutputDelegate^ outputDelegate)
		{
			try
			{
#if defined(debug)
				outputDelegate(String::Format(L"s{0}\t{1}", ConsoleApplication::CommandLineSwitchValueSeparator, value));
#endif

				//validate settings file path
				if (!System::IO::File::Exists(value))
				{
					throw gcnew System::ArgumentException(String::Format(L"Invalid settings file path: '{0}'", value));
				}
				Globals::setFilename(value);
			}
			catch (Exception^ ex)
			{
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);
				throw;
			}
		}
		#pragma endregion CommandLineSwitch Action Delegates
		#pragma endregion Methods
}
