//AssemblyInfo.h
#pragma once
 
#define Assembly_Copyright L"Copyright (C) 1989, 1991 Free Software Foundation, Inc.  \n59 Temple Place - Suite 330, Boston, MA  02111-1307, USA"
#define Assembly_Description L"Reference implementation of MVC, Forms"
#define File_Version 0.3.0.0
#define File_Version_Str "0.3.0.0"
 
//CLR assembly version
#define Assembly_Version L"0.3.0.0"

#pragma region " Helper class to get information for the About form. "
/// <summary>
/// This class uses the System.Reflection.Assembly class to
/// access assembly meta-data
/// This class is ! a normal feature of AssemblyInfo.cs
/// </summary>
	// Used by Helper Functions to access information from Assembly Attributes
public ref class AssemblyInfo : public AssemblyInfoBase
{
public:
		AssemblyInfo();

};
#pragma endregion
