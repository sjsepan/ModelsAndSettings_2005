﻿//MVCViewModel.cpp

#include "stdafx.h"
#include "MVCViewModel.h"

namespace MVCFormsCpp
{
		#pragma region Declarations
		#pragma endregion Declarations

		#pragma region Constructors
		MVCViewModel::MVCViewModel()
		{
		} //Note: not called, but need to be present to compile--SJS

		MVCViewModel::MVCViewModel(PropertyChangedEventHandler^ propertyChangedEventHandlerDelegate, Dictionary<String^, Bitmap^>^ actionIconImages, FileDialogInfo^ settingsFileDialogInfo)
		{
			try
			{
				FormsViewModel(propertyChangedEventHandlerDelegate, actionIconImages, settingsFileDialogInfo);
			}
			catch (Exception^ ex)
			{
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);
			}
		}
		#pragma endregion Constructors

		#pragma region Properties
		#pragma endregion Properties

		#pragma region Methods
		/// <summary>
		/// model specific, not generic
		/// </summary>
		void MVCViewModel::DoSomething()
		{
			this->StatusMessage = L"";
			this-> ErrorMessage = L"";

			try
			{
				StartProgressBar(L"Doing something...", L"", nullptr, true, 33);

				ModelController<MVCModel^>::Model->setSomeBoolean(!ModelController<MVCModel^>::Model->getSomeBoolean());
				ModelController<MVCModel^>::Model->setSomeInt(ModelController<MVCModel^>::Model->getSomeInt() + 1); //+= 1;
//C# TO C++ CONVERTER TODO TASK: There is no C++ equivalent to 'ToString':
				ModelController<MVCModel^>::Model->setSomeString(DateTime::Now.ToString());

				ModelController<MVCModel^>::Model->getSomeComponent()->setSomeOtherBoolean(!ModelController<MVCModel^>::Model->getSomeComponent()->getSomeOtherBoolean());
				ModelController<MVCModel^>::Model->getSomeComponent()->setSomeOtherInt(ModelController<MVCModel^>::Model->getSomeComponent()->getSomeOtherInt() + 1); //+= 1;
//C# TO C++ CONVERTER TODO TASK: There is no C++ equivalent to 'ToString':
				ModelController<MVCModel^>::Model->getSomeComponent()->setSomeOtherString(DateTime::Now.ToString());

				ModelController<MVCModel^>::Model->getStillAnotherComponent()->setStillAnotherBoolean(!ModelController<MVCModel^>::Model->getStillAnotherComponent()->getStillAnotherBoolean());
				ModelController<MVCModel^>::Model->getStillAnotherComponent()->setStillAnotherInt(ModelController<MVCModel^>::Model->getStillAnotherComponent()->getStillAnotherInt() + 1); //+= 1;
//C# TO C++ CONVERTER TODO TASK: There is no C++ equivalent to 'ToString':
				ModelController<MVCModel^>::Model->getStillAnotherComponent()->setStillAnotherString(DateTime::Now.ToString());

				UpdateStatusBarMessages(L"", L"", DateTime::Now.ToLongTimeString());

				ModelController<MVCModel^>::Model->Refresh();
			}
			catch (Exception^ ex)
			{
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);

				StopProgressBar(L"", String::Format(L"{0}", ex->Message));
			}
//C# TO C++ CONVERTER TODO TASK: There is no C++ equivalent to the exception 'finally' clause:
			//finally
			//{
				StopProgressBar(L"Did something.");
			//}
		}
		#pragma endregion Methods

}
