//MVCViewModel.h

#pragma once

#include "FormView.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections::Generic;
//using System.Collections.ObjectModel;
using namespace System::Diagnostics;
using namespace System::Drawing;
using namespace System::Reflection;
using namespace System::Windows::Forms;
using namespace Ssepan::Utility;
using namespace Ssepan::Application;
using namespace Ssepan::Application::MVC;
using namespace Ssepan::Application::WinForms;
using namespace Ssepan::Io;
using namespace MVCLibraryCpp;

#using ".\Debug\FormView.obj"

ref class FormView;

namespace MVCFormsCpp
{
	/// <summary>
	/// Note: this class can subclass the base without type parameters.
	/// </summary>
	public ref class MVCViewModel : public FormsViewModel<Bitmap^, MVCSettings^, MVCModel^, Form^/*FormView^*/>
	{

		#pragma region Declarations
		#pragma endregion Declarations

		#pragma region Constructors
	public:
		MVCViewModel();

		MVCViewModel(PropertyChangedEventHandler^ propertyChangedEventHandlerDelegate, Dictionary<String^, Bitmap^>^ actionIconImages, FileDialogInfo^ settingsFileDialogInfo); //: FormsViewModel(propertyChangedEventHandlerDelegate, actionIconImages, settingsFileDialogInfo)
		#pragma endregion Constructors

		#pragma region Properties
		#pragma endregion Properties

		#pragma region Methods
	public:
		/// <summary>
		/// model specific, not generic
		/// </summary>
		void DoSomething();
		#pragma endregion Methods
	};
}