//Program.h

#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Diagnostics;
using namespace System::Reflection;
using namespace Ssepan::Application;
using namespace Ssepan::Application::MVC;
using namespace Ssepan::Application::WinConsole;
using namespace Ssepan::Utility;
using namespace MVCLibraryCpp;
using namespace VCPPLIBRARYCLR;

#using ".\Debug\ConsoleView.obj"

ref class ConsoleView;

namespace MVCConsoleCpp
{
	//Note: wasn't static originally; changed to match winforms
	public ref class Program   : public INotifyPropertyChanged
	{

		#pragma region Declarations
			static Program^ programInstance;
		#pragma endregion Declarations

		#pragma region Constructors
	public:
		virtual ~Program();

		Program();
		#pragma endregion Constructors

		#pragma region INotifyPropertyChanged
	public:
		virtual event PropertyChangedEventHandler^ PropertyChanged;
		static event PropertyChangedEventHandler^ PropertyChangedStatic;
	protected:
		static void OnPropertyChanged(String^ propertyName);
		#pragma endregion INotifyPropertyChanged

		#pragma region PropertyChangedEventHandlerDelegate
		/// <summary>
		/// Note: property changes update UI manually.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		static void PropertyChangedEventHandlerDelegate(Object^ sender, PropertyChangedEventArgs^ e);
		#pragma endregion PropertyChangedEventHandlerDelegate

		#pragma region Properties
		#pragma endregion Properties

		#pragma region Methods
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		/// <param name="args"></param>
		/// <returns>Int32</returns>
//C# TO C++ CONVERTER NOTE: The following .NET attribute has no direct equivalent in C++:
//ORIGINAL LINE: [STAThread] static Int32 Main(String[] args)
		//[STAThreadAttribute]
	public:
		static Int32 Main(array<String^>^ args);

		#pragma region ConsoleAppBase
		/// <summary>
		/// Note: switches are processed before Model or Settings are accessed.
		/// </summary>
		/// <param name="args"></param>
	private:
		static void DoSwitches(array<String^>^ args);

		//Note:model, Settings init done in viewmodel (after default handler set)
		#pragma endregion ConsoleAppBase

		#pragma region CommandLineSwitch Action Delegates
		/// <summary>
		/// Instance of an action conforming to delegate Action(Of TSettings), where TSettings is String.
		/// Command 't' tests the use of parameters.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="outputDelegate"></param>
		static void t(String^ value, ConsoleApplication::TOutputDelegate^ outputDelegate);

		/// <summary>
		/// Validate and set selected settings.
		/// Instance of an action conforming to delegate Action<T>, where T is String.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="outputDelegate"></param>
		static void f(String^ value, ConsoleApplication::TOutputDelegate^ outputDelegate);
		#pragma endregion CommandLineSwitch Action Delegates
		#pragma endregion Methods

	};
}
