﻿//ConsoleView.cpp

#include "stdafx.h"
#include "ConsoleView.h"

namespace MVCConsoleCpp
{
		#pragma region Declarations
		#pragma endregion Declarations

		#pragma region Constructors
		ConsoleView::~ConsoleView()
		{
			delete ViewModel;
		}

		ConsoleView::ConsoleView()
		{
			try
			{
				_ViewName = Globals::APP_NAME;

				////(re)define default output delegate
				//ConsoleApplication.defaultOutputDelegate = ConsoleApplication.writeLineWrapperOutputDelegate;

				//subscribe view to notifications
				//this->PropertyChanged += gcnew PropertyChangedEventHandler(this, ConsoleView::PropertyChangedEventHandlerDefinition);
				this->PropertyChanged += PropertyChangedEventHandlerDelegate;

				InitViewModel();
			}
			catch (Exception^ ex)
			{
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);
			}
		}
		#pragma endregion Constructors

		#pragma region IDisposable
////C# TO C++ CONVERTER WARNING: There is no equivalent in C++ to finalizer methods:
//		void ConsoleView::Finalize()
//		{
//			Dispose(false);
//		}
//
//		void ConsoleView::Dispose()
//		{
//			delete ViewModel;
//			delete ViewModel;
//			// dispose of the managed and unmanaged resources
//			Dispose(true);
//
//			// tell the GC that the Finalize process no longer needs
//			// to be run for this object.
////C# TO C++ CONVERTER WARNING: There is no garbage collector in C++:
////			GC::SuppressFinalize(this);
//		}
//
//		void ConsoleView::Dispose(bool disposeManagedResources)
//		{
//			// process only if mananged and unmanaged resources have
//			// not been disposed of.
//			if (!this->disposed)
//			{
//				//Resources not disposed
//				if (disposeManagedResources)
//				{
//					// dispose managed resources
//					//unsubscribe from model notifications
//					if (this->PropertyChanged != nullptr)
//					{
//						this->PropertyChanged->removeListener(L"PropertyChangedEventHandlerDelegate");
//					}
//				}
//				// dispose unmanaged resources
//				disposed = true;
//			}
//			else
//			{
//				//Resources already disposed
//			}
//		}
		#pragma endregion IDisposable

		#pragma region INotifyPropertyChanged
		void ConsoleView::OnPropertyChanged(String^ propertyName)
		{
			try
			{
				ConsoleView::PropertyChangedEventHandlerDefinition(nullptr, gcnew PropertyChangedEventArgs(propertyName));
			}
			catch (Exception^ ex)
			{
				ViewModel->ErrorMessage = ex->Message;
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);

				throw;
			}
		}
		#pragma endregion INotifyPropertyChanged

		#pragma region PropertyChangedEventHandlerDelegate
		/// <summary>
		/// Note: model property changes update UI manually.
		/// Note: handle settings property changes manually.
		/// Note: because settings properties are a subset of the model 
		///  (every settings property should be in the model, 
		///  but not every model property is persisted to settings)
		///  it is decided that for now the settigns handler will 
		///  invoke the model handler as well.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void ConsoleView::PropertyChangedEventHandlerDefinition(Object^ sender, PropertyChangedEventArgs^ e)
		{
			try
			{
#pragma region Model
				if (e->PropertyName == L"IsChanged")
				{
					//ConsoleApplication.defaultOutputDelegate(String.Format("{0}", e.PropertyName));
					ConsoleView::ApplySettings();
				}
				else if (e->PropertyName == L"Progress")
				{
					ConsoleApplication::defaultOutputDelegate(String::Format(L"{0}", getProgress()));
				}
				if (e->PropertyName == L"StatusMessage")
				{
					ConsoleApplication::defaultOutputDelegate(String::Format(L"{0}", ViewModel->StatusMessage));
					e = gcnew PropertyChangedEventArgs(e->PropertyName + L".handled");
				}
				else if (e->PropertyName == L"ErrorMessage")
				{
					ConsoleApplication::defaultOutputDelegate(String::Format(L"{0}", ViewModel->ErrorMessage));
					e = gcnew PropertyChangedEventArgs(e->PropertyName + L".handled");
				}
				//Note: not databound, so handle event
				else if (e->PropertyName == L"SomeInt")
				{
					ConsoleApplication::defaultOutputDelegate(String::Format(L"SomeInt: {0}", ModelController<MVCModel^>::Model->getSomeInt()));
				}
				else if (e->PropertyName == L"SomeBoolean")
				{
					ConsoleApplication::defaultOutputDelegate(String::Format(L"SomeBoolean: {0}", ModelController<MVCModel^>::Model->getSomeBoolean()));
				}
				else if (e->PropertyName == L"SomeString")
				{
					ConsoleApplication::defaultOutputDelegate(String::Format(L"SomeString: {0}", ModelController<MVCModel^>::Model->getSomeString()));
				}
				else if (e->PropertyName == L"SomeOtherInt")
				{
					ConsoleApplication::defaultOutputDelegate(String::Format(L"SomeOtherInt: {0}", ModelController<MVCModel^>::Model->getSomeComponent()->getSomeOtherInt()));
				}
				else if (e->PropertyName == L"SomeOtherBoolean")
				{
					ConsoleApplication::defaultOutputDelegate(String::Format(L"SomeOtherBoolean: {0}", ModelController<MVCModel^>::Model->getSomeComponent()->getSomeOtherBoolean()));
				}
				else if (e->PropertyName == L"SomeOtherString")
				{
					ConsoleApplication::defaultOutputDelegate(String::Format(L"SomeOtherString: {0}", ModelController<MVCModel^>::Model->getSomeComponent()->getSomeOtherString()));
				}
				else if (e->PropertyName == L"SomeComponent")
				{
					ConsoleApplication::defaultOutputDelegate(String::Format(L"SomeComponent: {0},{1},{2}", ModelController<MVCModel^>::Model->getSomeComponent()->getSomeOtherInt(), ModelController<MVCModel^>::Model->getSomeComponent()->getSomeOtherBoolean(), ModelController<MVCModel^>::Model->getSomeComponent()->getSomeOtherString()));
				}
				else if (e->PropertyName == L"StillAnotherInt")
				{
					ConsoleApplication::defaultOutputDelegate(String::Format(L"StillAnotherInt: {0}", ModelController<MVCModel^>::Model->getStillAnotherComponent()->getStillAnotherInt()));
				}
				else if (e->PropertyName == L"StillAnotherBoolean")
				{
					ConsoleApplication::defaultOutputDelegate(String::Format(L"StillAnotherBoolean: {0}", ModelController<MVCModel^>::Model->getStillAnotherComponent()->getStillAnotherBoolean()));
				}
				else if (e->PropertyName == L"StillAnotherString")
				{
					ConsoleApplication::defaultOutputDelegate(String::Format(L"StillAnotherString: {0}", ModelController<MVCModel^>::Model->getStillAnotherComponent()->getStillAnotherString()));
				}
				else if (e->PropertyName == L"StillAnotherComponent")
				{
					ConsoleApplication::defaultOutputDelegate(String::Format(L"StillAnotherComponent: {0},{1},{2}", ModelController<MVCModel^>::Model->getStillAnotherComponent()->getStillAnotherInt(), ModelController<MVCModel^>::Model->getStillAnotherComponent()->getStillAnotherBoolean(), ModelController<MVCModel^>::Model->getStillAnotherComponent()->getStillAnotherString()));
				}
				else
				{
#ifdef DEBUG_MODEL_PROPERTYCHANGED
						ConsoleApplication::defaultOutputDelegate(String::Format(L"e.PropertyName: {0}", e->PropertyName));
#endif
				}
#pragma endregion Model

#pragma region Settings
				if (e->PropertyName == L"Dirty")
				{
					//apply settings that don't have databindings
					ViewModel->DirtyIconIsVisible = (SettingsController<MVCSettings^>::Settings->Dirty);
				}
				else
				{
#ifdef DEBUG_SETTINGS_PROPERTYCHANGED
					ConsoleApplication::defaultOutputDelegate(String::Format(L"e.PropertyName: {0}", e->PropertyName));
#endif
				}
#pragma endregion Settings
			}
			catch (Exception^ ex)
			{
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);
			}
		}
#pragma endregion PropertyChangedEventHandlerDelegate

#pragma region Properties
		String^ ConsoleView::getViewName()
		{
			return _ViewName;
		}
		void ConsoleView::setViewName(String^ value)
		{
			_ViewName = value;
			OnPropertyChanged(L"ViewName");
		}


		Int32 ConsoleView::getProgress()
		{
			return _Progress;
		}
		void ConsoleView::setProgress(Int32 value)
		{
			_Progress = value;
			OnPropertyChanged(L"Progress");
		}
#pragma endregion Properties

#pragma region Methods
#pragma region ConsoleAppBase
		void ConsoleView::InitViewModel()
		{
			Dictionary<String^, String^>^ d;
			try
			{
				//subscribe model controller to notifications
				//tell controller how model should notify view about non-persisted properties AND including model properties that may be part of settings
				ModelController<MVCModel^>::DefaultHandler =  gcnew PropertyChangedEventHandler( ConsoleView::PropertyChangedEventHandlerDefinition);

				//subscribe settings controller to notifications
				//tell controller how settings should notify view about persisted properties
				SettingsController<MVCSettings^>::DefaultHandler =  gcnew PropertyChangedEventHandler( this->PropertyChangedEventHandlerDefinition);

				ConsoleView::InitModelAndSettings();

#ifdef USE_CUSTOM_VIEWMODEL
				//class to handle standard behaviors
				d = gcnew Dictionary<String^, String^>();
				d->Add(L"New", L"New");
				d->Add(L"Save", L"Save");
				d->Add(L"Open", L"Open");
				d->Add(L"Print", L"Print");
				d->Add(L"Copy", L"Copy");
				d->Add(L"Properties", L"Properties");

				//subscribe viewmodel controller to notifications
				ViewModelController<String^, MVCConsoleViewModel^>::New
				(
					ConsoleView::getViewName(), 
					gcnew MVCConsoleViewModel
					(
						gcnew PropertyChangedEventHandler( this->PropertyChangedEventHandlerDefinition), 
						d
					)
				);

				//select a viewmodel by view name
				ViewModel = ViewModelController<String^, MVCConsoleViewModel^>::ViewModel[ConsoleView::getViewName()];
#endif
#ifndef USE_CUSTOM_VIEWMODEL
				//class to handle standard behaviors
				ConsoleViewModel<String^, MVCSettings^, MVCModel^> tempVar2(this->PropertyChangedEventHandlerDelegate, gcnew Dictionary<String^, String^>
				{
					{L"New", L"New"},
					{L"Save", L"Save"},
					{L"Open", L"Open"},
					{L"Print", L"Print"},
					{L"Copy", L"Copy"},
					{L"Properties", L"Properties"}
				});
				ViewModelController<String^, ConsoleViewModel<String^, MVCSettings^, MVCModel^>^>::New(ViewName, %tempVar2);

				//select a viewmodel by view name
				ViewModel = ViewModelController<String^, ConsoleViewModel<String^, MVCSettings^, MVCModel^>^>::ViewModel[ViewName];
#endif

				//Init config parameters
				if (!LoadParameters())
				{
					throw gcnew Exception(String::Format(L"Unable to load config file parameter(s)."));
				}

				//DEBUG:filename coming in is being converted/passed as DOS 8.3 format equivalent
				//Load
				if ((SettingsController<MVCSettings^>::FilePath == nullptr) || (SettingsController<MVCSettings^>::Filename == SettingsController<MVCSettings^>::FILE_NEW))
				{
					//NEW
					ViewModel->FileNew();
				}
				else
				{
					//OPEN
					ViewModel->FileOpen();
				}
			}
			catch (Exception^ ex)
			{
				if (ViewModel != nullptr)
				{
					ViewModel->ErrorMessage = ex->Message;
				}
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);
			}
		}

		void ConsoleView::InitModelAndSettings()
		{
			//create Settings before first use by Model
			if (SettingsController<MVCSettings^>::Settings == nullptr)
			{
				SettingsController<MVCSettings^>::New();
			}
			//Model properties rely on Settings, so don't call Refresh before this is run.
			if (ModelController<MVCModel^>::Model == nullptr)
			{
				ModelController<MVCModel^>::New();
			}
		}

		void ConsoleView::DisposeSettings()
		{

			//save user and application settings 
			//Properties.Settings.Default.Save();

			if (SettingsController<MVCSettings^>::Settings->Dirty)
			{
				//SAVE
				ViewModel->FileSave();
			}

			//unsubscribe from model notifications
			ModelController<MVCModel^>::Model->PropertyChanged -= gcnew PropertyChangedEventHandler( this->PropertyChangedEventHandlerDefinition);
		}

		Int32 ConsoleView::_Main()
		{
			Int32 returnValue = -1; //default to fail code

			try
			{
				ViewModel->StatusMessage = String::Format(L"{0} starting...", ConsoleView::getViewName());

				//init settigns

				ViewModel->StatusMessage = String::Format(L"{0} started.", ConsoleView::getViewName());

				_Run();

				ViewModel->StatusMessage = String::Format(L"{0} completing...", ConsoleView::getViewName());

				DisposeSettings();

				ViewModel->StatusMessage = String::Format(L"{0} completed.", ConsoleView::getViewName());

				//return success code
				returnValue = 0;
			}
			catch (Exception^ ex)
			{
				ViewModel->ErrorMessage = String::Format(L"{0} did NOT complete: '{1}'", ConsoleView::getViewName(), ViewModel->ErrorMessage);
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);
			}
//C# TO C++ CONVERTER TODO TASK: There is no C++ equivalent to the exception 'finally' clause:
			finally
			{
				ViewModel = nullptr;
			}
			return returnValue;
		}

		void ConsoleView::_Run()
		{
			ViewModel->DoSomething();

		}
#pragma endregion ConsoleAppBase

#pragma region Utility
		/// <summary>
		/// Apply Settings to viewer.
		/// </summary>
		void ConsoleView::ApplySettings()
		{
			try
			{
				_ValueChangedProgrammatically = true;

				//apply settings that have databindings
				//BindModelUi();

				//apply settings that shouldn't use databindings

				//apply settings that can't use databindings
				Console::Title = Path::GetFileName(SettingsController<MVCSettings^>::Filename) + L" - " + ConsoleView::getViewName();

				//apply settings that don't have databindings
				//ViewModel.StatusBarDirtyMessage.Visible = (SettingsController<Settings>.Settings.Dirty);

				_ValueChangedProgrammatically = false;
			}
			catch (Exception^ ex)
			{
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);
				throw;
			}
		}

		/// <summary>
		/// Load from app config; override with command line if present
		/// </summary>
		/// <returns></returns>
		bool ConsoleView::LoadParameters()
		{
			bool returnValue = false;
#ifdef USE_CONFIG_FILEPATH
			String^ _settingsFilename = L"";
#endif

			try
			{
				if ((Globals::getFilename() != nullptr) && (Globals::getFilename() != SettingsController<MVCSettings^>::FILE_NEW))
				{
					//got filename from command line
					//DEBUG:filename coming in is being converted/passed as DOS 8.3 format equivalent
					if (!RegistryAccess::ValidateFileAssociation(Application::ExecutablePath, L"." + MVCSettings::FileTypeExtension))
					{
						throw gcnew ApplicationException(String::Format(L"Settings filename not associated: '{0}'.\nCheck filename on command line.", Globals::getFilename()));
					}
					//it passed; use value from command line
				}
				else
				{
#ifdef USE_CONFIG_FILEPATH
					//get filename from app.config
					if (!Ssepan::Utility::Configuration::ReadString(L"SettingsFilename", _settingsFilename))
					{
						throw gcnew ApplicationException(String::Format(L"Unable to load SettingsFilename: {0}", L"SettingsFilename"));
					}
					if ((_settingsFilename == L"") || (_settingsFilename == SettingsController<MVCSettings^>::FILE_NEW))
					{
						throw gcnew ApplicationException(String::Format(L"Settings filename not set: '{0}'.\nCheck SettingsFilename in app config file.", _settingsFilename));
					}
					//use with the supplied path
					SettingsController<MVCSettings^>::Filename = _settingsFilename;

					if (Path::GetDirectoryName(_settingsFilename) == L"")
					{
						//supply default path if missing
						SettingsController<MVCSettings^>::Pathname = PathExtensions::WithTrailingSeparator(Environment::GetFolderPath(Environment::SpecialFolder::Personal));
					}
#endif
				}

				returnValue = true;
			}
			catch (Exception^ ex)
			{
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);
				//throw;
			}
			return returnValue;
		}
#pragma endregion Utility
#pragma endregion Methods
}
