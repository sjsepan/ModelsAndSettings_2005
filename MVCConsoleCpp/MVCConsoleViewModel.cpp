﻿//MVCConsoleViewModel.cpp

#include "stdafx.h"
#include "MVCConsoleViewModel.h"

namespace MVCConsoleCpp
{
		#pragma region Declarations
		#pragma endregion Declarations

		#pragma region Constructors
		MVCConsoleViewModel::MVCConsoleViewModel()
		{
		} //Note: not called, but need to be present to compile--SJS

		MVCConsoleViewModel::MVCConsoleViewModel(PropertyChangedEventHandler^ propertyChangedEventHandlerDelegate, Dictionary<String^, String^>^ actionIconImages) 
		{
			try
			{	//TODO: base is ConsoleViewModel<string,MVCSettings,MVCModel>
				//Ssepan::Application::WinConsole::ConsoleViewModel(propertyChangedEventHandlerDelegate, actionIconImages);//base
			}
			catch (Exception^ ex)
			{
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);
			}
		}
		#pragma endregion Constructors

		#pragma region Properties
		#pragma endregion Properties

		#pragma region Methods
		/// <summary>
		/// model specific, not generic
		/// </summary>
		void MVCConsoleViewModel::DoSomething() 
		{
			StatusMessage = L"";
			ErrorMessage = L"";

			try
			{
				this->StartProgressBar(L"Doing something...", L"", L"", true, 33);

				ModelController<MVCModel^>::Model->setSomeBoolean(!ModelController<MVCModel^>::Model->getSomeBoolean());
				ModelController<MVCModel^>::Model->setSomeInt(ModelController<MVCModel^>::Model->getSomeInt() + 1);
				ModelController<MVCModel^>::Model->setSomeString(DateTime::Now.ToString());

				ModelController<MVCModel^>::Model->getSomeComponent()->setSomeOtherBoolean(!ModelController<MVCModel^>::Model->getSomeComponent()->getSomeOtherBoolean());
				ModelController<MVCModel^>::Model->getSomeComponent()->setSomeOtherInt(ModelController<MVCModel^>::Model->getSomeComponent()->getSomeOtherInt() + 1);
				ModelController<MVCModel^>::Model->getSomeComponent()->setSomeOtherString(DateTime::Now.ToString());

				ModelController<MVCModel^>::Model->getStillAnotherComponent()->setStillAnotherBoolean(!ModelController<MVCModel^>::Model->getStillAnotherComponent()->getStillAnotherBoolean());
				ModelController<MVCModel^>::Model->getStillAnotherComponent()->setStillAnotherInt(ModelController<MVCModel^>::Model->getStillAnotherComponent()->getStillAnotherInt() + 1);
				ModelController<MVCModel^>::Model->getStillAnotherComponent()->setStillAnotherString(DateTime::Now.ToString());

				ModelController<MVCModel^>::Model->Refresh();
			}
			catch (Exception^ ex)
			{
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);

				StopProgressBar(L"", String::Format(L"{0}", ex->Message));
			}
//C# TO C++ CONVERTER TODO TASK: There is no C++ equivalent to the exception 'finally' clause:
			finally
			{
				StopProgressBar(L"\nDid something.");
			}
		}
		#pragma endregion Methods

}
