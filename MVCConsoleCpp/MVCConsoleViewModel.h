//MVCConsoleViewModel.h

#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Diagnostics;
using namespace System::Reflection;
using namespace System::Collections::Generic;
using namespace Ssepan::Utility;
using namespace Ssepan::Application;
using namespace Ssepan::Application::MVC;
using namespace Ssepan::Application::WinConsole;
using namespace Ssepan::Io;
using namespace MVCLibraryCpp;

//#using "..\MVCLibraryCpp\Debug\MVCModel.obj"
//#using "..\MVCLibraryCpp\Debug\MVCModelComponent.obj"
//#using "..\MVCLibraryCpp\Debug\MVCSettings.obj"
//#using "..\MVCLibraryCpp\Debug\MVCSettingsComponent.obj"

//ref class MVCModel;
//ref class MVCModelComponent;
//ref class MVCSettings;
//ref class MVCSettingsComponent;

namespace MVCConsoleCpp
{
	/// <summary>
	/// Note: this class can subclass the base without type parameters.
	/// </summary>
	public ref class MVCConsoleViewModel : public ConsoleViewModel<String^, MVCSettings^, MVCModel^>
	{
		#pragma region Declarations
		#pragma endregion Declarations

		#pragma region Constructors
	public:
		MVCConsoleViewModel();

		MVCConsoleViewModel(PropertyChangedEventHandler^ propertyChangedEventHandlerDelegate, Dictionary<String^, String^>^ actionIconImages);
		#pragma endregion Constructors

		#pragma region Properties
		#pragma endregion Properties

		#pragma region Methods
	public:
		/// <summary>
		/// model specific, not generic
		/// </summary>
		void DoSomething();
		#pragma endregion Methods


	};
}