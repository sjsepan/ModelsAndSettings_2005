========================================================================
    APPLICATION : MVCConsoleCpp Project Overview
========================================================================

AppWizard has created this MVCConsoleCpp Application for you.  

This file contains a summary of what you will find in each of the files that
make up your MVCConsoleCpp application.

MVCConsoleCpp.vcproj
    This is the main project file for VC++ projects generated using an Application Wizard. 
    It contains information about the version of Visual C++ that generated the file, and 
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

MVCConsoleCpp.cpp
    This is the main application source file.

AssemblyInfo.cpp
	Contains custom attributes for modifying assembly metadata.

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" to indicate parts of the source code you
should add to or customize.

/////////////////////////////////////////////////////////////////////////////
