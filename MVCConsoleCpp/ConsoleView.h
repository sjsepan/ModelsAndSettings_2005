//ConsoleView.h

#pragma once

#define USE_CONFIG_FILEPATH
#define USE_CUSTOM_VIEWMODEL
//#define DEBUG_MODEL_PROPERTYCHANGED
//#define DEBUG_SETTINGS_PROPERTYCHANGED

using namespace System;
using namespace System::Collections::Generic;
using namespace System::ComponentModel;
using namespace System::Diagnostics;
using namespace System::IO;
using namespace System::Reflection;
using namespace System::Windows::Forms;
using namespace Ssepan::Application;
using namespace Ssepan::Application::MVC;
using namespace Ssepan::Application::WinConsole;
using namespace Ssepan::Io;
using namespace Ssepan::Utility;
using namespace MVCLibraryCpp;

#using ".\Debug\Globals.obj"
#using ".\Debug\MVCConsoleViewModel.obj"

ref class Globals;
ref class MVCConsoleViewModel;

namespace MVCConsoleCpp
{
	public ref class ConsoleView /*: public INotifyPropertyChanged*/
	{
		#pragma region Declarations
	protected:
		bool disposed;
#ifdef USE_CUSTOM_VIEWMODEL
		static MVCConsoleViewModel^ ViewModel;
#endif
#ifndef USE_CUSTOM_VIEWMODEL
		ConsoleViewModel<String^, MVCSettings^, MVCModel^>^ ViewModel = nullptr;
#endif
	private:
		static bool _ValueChangedProgrammatically;
		#pragma endregion Declarations

		#pragma region Constructors
	public:
		virtual ~ConsoleView();

		ConsoleView();
		#pragma endregion Constructors

		#pragma region IDisposable
//	private:
////C# TO C++ CONVERTER WARNING: There is no equivalent in C++ to finalizer methods:
//		void Finalize();
//
//	public:
//		virtual void Dispose();
//
//	protected:
//		virtual void Dispose(bool disposeManagedResources);
		#pragma endregion IDisposable

		#pragma region INotifyPropertyChanged
	public:
		event PropertyChangedEventHandler^ PropertyChanged;
	protected:
		static void OnPropertyChanged(String^ propertyName);
		#pragma endregion INotifyPropertyChanged

		#pragma region PropertyChangedEventHandlerDelegate
	public:
	protected:
		/// <summary>
		/// Note: model property changes update UI manually.
		/// Note: handle settings property changes manually.
		/// Note: because settings properties are a subset of the model 
		///  (every settings property should be in the model, 
		///  but not every model property is persisted to settings)
		///  it is decided that for now the settigns handler will 
		///  invoke the model handler as well.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		static void PropertyChangedEventHandlerDefinition(Object^ sender, PropertyChangedEventArgs^ e);
#pragma endregion PropertyChangedEventHandlerDelegate

#pragma region Properties
	private:
		static String^ _ViewName;
	public:
		static String^ getViewName();

		static void setViewName(String^ value);


	private:
		static Int32 _Progress;
	public:
		static Int32 getProgress();

		static void setProgress(Int32 value);
#pragma endregion Properties

#pragma region Methods
#pragma region ConsoleAppBase
	protected:
		void InitViewModel();

		void InitModelAndSettings();

		void DisposeSettings();

	public:
		Int32 _Main();

	protected:
		void _Run();
#pragma endregion ConsoleAppBase

#pragma region Utility
		/// <summary>
		/// Apply Settings to viewer.
		/// </summary>
	private:
		static void ApplySettings();

		/// <summary>
		/// Load from app config; override with command line if present
		/// </summary>
		/// <returns></returns>
		bool LoadParameters();
#pragma endregion Utility
#pragma endregion Methods
	};

}