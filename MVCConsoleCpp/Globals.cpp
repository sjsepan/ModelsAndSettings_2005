//Globals.cpp

#include "stdafx.h"
#include "Globals.h"

//namespace MVCConsoleCpp
//{
		#pragma region Declarations
		#pragma endregion Declarations

		#pragma region Constructors
		Globals::Globals()
		{
			try
			{
				APP_NAME = L"MVCConsoleCpp";
			}
			catch (Exception^ ex)
			{
				Log::Write(ex, MethodBase::GetCurrentMethod(), EventLogEntryType::Error);
			}
		}
		#pragma endregion Constructors

		#pragma region Properties
		String^ Globals::getFilename()
		{
			return _Filename;
		}
		void Globals::setFilename(String^ value)
		{
			_Filename = value;
			//OnPropertyChanged(L"Filename");
		}
		#pragma endregion Properties
//}
